import React from 'react';
import { Icon } from 'semantic-ui-react';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: absolute;
  top: 15px;
  left: 15px;
`;

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
`;

const Hamburger = () =>
  <Wrapper>
    <Button>
      <Icon name="sidebar" size="large" inverted />
    </Button>
  </Wrapper>;

export default Hamburger;
