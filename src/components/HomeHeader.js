import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import logo from '../logo.png';

const Wrapper = styled.div`
  width: 100%;
  height: 140px;
  color: #189963;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  padding-top: 24px;
`;

const Logo = styled.img.attrs({
  src: logo,
  alt: 'NutrifyMe logo',
})`
  height: 50px;
`;

const Brand = styled.h1`letter-spacing: 0.1rem;`;

class HomeHeader extends Component {
  render() {
    return (
      <Wrapper>
        <NavLink exact to="/">
          <Logo />
        </NavLink>
        <Brand>NutrifyMe</Brand>
      </Wrapper>
    );
  }
}

export default HomeHeader;
