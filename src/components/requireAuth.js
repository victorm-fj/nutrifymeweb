import React, { Component } from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';
import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';

import { setDate } from '../actions';
import currentUserQuery from '../queries/CurrentUser';
import userProfileQuery from '../queries/UserProfile';
import Evaluation from '../evaluation';

export default WrappedComponent => {
  class RequireAuth extends Component {
    componentWillUpdate(nextProps) {
      if (!nextProps.userQuery.loading && !nextProps.userQuery.user) {
        this.props.history.push('/');
      }
    }

    render() {
      const { userQuery, profileQuery } = this.props;

      if (userQuery.loading || profileQuery.loading) {
        return (
          <Dimmer active inverted>
            <Loader inverted>Loading</Loader>
          </Dimmer>
        );
      }

      if (!profileQuery.profile) {
        return <Evaluation />;
      }

      return <WrappedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => {
    return {
      currentDate: state.general.date,
      activeMeal: state.general.activeMeal,
    };
  };

  return compose(
    connect(mapStateToProps, { setDate }),
    graphql(currentUserQuery, { name: 'userQuery' }),
    graphql(userProfileQuery, {
      name: 'profileQuery',
      options: ownProps => ({ variables: { date: ownProps.currentDate } }),
    })
  )(RequireAuth);
};
