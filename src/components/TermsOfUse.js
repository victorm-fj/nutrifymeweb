import React, { Component } from 'react';
import styled from 'styled-components';

import HomeHeader from './HomeHeader';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  margin: 0 auto 100px;
`;

class TermsOfUse extends Component {
  render() {
    return (
      <Wrapper>
        <HomeHeader />

        <h2>Terms of Use</h2>

        <h3>Conditions</h3>
        <p>Users</p>
        <p>
          Users of NutrifyMe must be at least eighteen (18) years of age to use
          this application.
        </p>

        <h3>Disclaimer</h3>
        <p>
          The content of this application is for informational purposes only.
          The content is not intended to be a substitute for professional
          medical advice, diagnosis, or treatment. Always seek the advice of
          your physician or other qualified health provider with any question
          you may have regarding a medical condition.
        </p>
        <p>
          NutrifyMe's developer does not directly or indirectly practice
          medicine or provide medical services and therefore assumes no
          liability whatsoever of any kind for the information and data accessed
          through this web application.
        </p>
      </Wrapper>
    );
  }
}

export default TermsOfUse;
