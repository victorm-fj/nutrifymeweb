import React, { Component } from 'react';
import { Tab, Button, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import { graphql } from 'react-apollo';

import mutation from '../mutations/UpdateProfile';
import query from '../queries/UserProfile';
import activityLevels from '../data/activityLevels.json';

const Wrapper = styled.div`
  flex: 1;
  width: 90%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 20px auto;

  @media (min-width: 500px) {
    width: 70%;
  }

  @media (min-width: 800px) {
    width: 50%;
  }

  @media (min-width: 950px) {
    width: 40%;
  }

  @media (min-width: 1440px) {
    width: 30%;
  }
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
  margin-bottom: 20px;
`;

const renderPane = ({ level, description }) =>
  <Tab.Pane style={{ color: '#757575' }}>
    <div>
      <h4>
        {level}
      </h4>
      <p>
        {description}
      </p>
    </div>
  </Tab.Pane>;

const panes = activityLevels.map(level => {
  return {
    menuItem: level.id,
    render: () => renderPane(level),
  };
});

class PhysicalActivity extends Component {
  state = { activeIndex: this.props.profile.activityLevel - 1, saving: false };

  onTabChangeHandler = (event, data) =>
    this.setState({ activeIndex: data.activeIndex });

  onClickHandler = () => {
    this.setState({ saving: true });

    this.props
      .mutate({
        variables: {
          id: this.props.profile.id,
          date: this.props.currentDate,
          activityLevel: this.state.activeIndex + 1,
        },
        refetchQueries: [
          {
            query,
            variables: {
              date: this.props.currentDate,
            },
          },
        ],
      })
      .then(() => {
        this.setState({ saving: false });
        this.props.close();
      })
      .catch(err => console.log(err));
  };

  render() {
    return (
      <Wrapper>
        {this.state.saving
          ? <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          : <div />}

        <Tab
          menu={{ pointing: true }}
          panes={panes}
          onTabChange={this.onTabChangeHandler}
          defaultActiveIndex={this.state.activeIndex}
        />

        <ButtonsWrapper>
          <Button basic color="red" onClick={this.props.close}>
            Cancel
          </Button>
          <Button basic color="violet" onClick={this.onClickHandler}>
            Save
          </Button>
        </ButtonsWrapper>
      </Wrapper>
    );
  }
}

export default graphql(mutation)(PhysicalActivity);
