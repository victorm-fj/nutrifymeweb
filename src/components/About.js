import React, { Component } from 'react';
import styled from 'styled-components';

import HomeHeader from './HomeHeader';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  margin: 0 auto 100px;
`;

class About extends Component {
  render() {
    return (
      <Wrapper>
        <HomeHeader />

        <h2>About</h2>

        <p>
          Simple calorie counter application that lets you keep track of the
          foods you eat.
        </p>

        <p>
          This application comes with an abbreviated pre-populated USDA food
          database with up to 8790 food records. Internet connection needed to
          make use of food search functionality.
        </p>

        <h3>Food Database</h3>
        <p>Source citation</p>
        <p>
          US Department of Agriculture, Agricultural Research Service, Nutrient
          Data Laboratory. USDA National Nutrient Database for Standard
          Reference, Release 28. Version Current: September 2015, slightly
          revised May 2016.
        </p>
        <p>Internet: /nea/bhnrc/ndl</p>

        <h3>Images</h3>
        <p>Application logo image</p>
        <p>Favicon logo image</p>
        <p>
          Icon made by Freepik from{' '}
          <a
            href="http://www.flaticon.com/"
            target="_blank"
            rel="noreferrer noopener"
          >
            www.flaticon.com
          </a>
        </p>
      </Wrapper>
    );
  }
}

export default About;
