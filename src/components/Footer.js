import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  height: 56px;
  padding-top: 80px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: auto;
`;

const LinksList = styled.ul`
  padding: 0;
  list-style: none;
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
  width: 100%;

  @media (min-width: 600px) {
    width: 80%;
  }

  @media (min-width: 800px) {
    width: 60%;
  }

  @media (min-width: 1200px) {
    width: 50%;
  }
`;

const ALink = styled(NavLink)`
  font-size: 12px;
  color: #757575;
  cursor: pointer;
`;

const Footer = () =>
  <Wrapper>
    <LinksList>
      <li>
        <ALink to="/about">About</ALink>
      </li>
      <li>
        <ALink to="/terms">Terms of Use</ALink>
      </li>
      <li>
        <ALink to="/privacy">Privacy Police</ALink>
      </li>
      <li>
        <ALink to="/cookie">Cookie Use</ALink>
      </li>
    </LinksList>
  </Wrapper>;

export default Footer;
