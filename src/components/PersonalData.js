import React, { Component } from 'react';
import { Form, Input, Select, Button, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import _ from 'lodash';
import { graphql } from 'react-apollo';

import mutation from '../mutations/UpdateProfile';
import profileQuery from '../queries/UserProfile';
import profilesQuery from '../queries/ProfileList';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 60px;
  margin-bottom: 20px;
`;

const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
];

class PersonalData extends Component {
  state = {
    age: this.props.profile.age,
    weight: this.props.profile.weight,
    height: this.props.profile.height,
    gender: this.props.profile.gender,
    saving: false,
  };

  setValue = (key, value) => {
    if (value === '') {
      this.setState({ [key]: value });
    } else {
      const num = Number(value);
      if (_.isFinite(num) && num > 0) {
        this.setState({ [key]: num });
      }
    }
  };

  onChangeHandler = (event, data) => {
    const { value, placeholder } = data;
    if (placeholder === 'years') this.setValue('age', value);
    else if (placeholder === 'kg') this.setValue('weight', value);
    else if (placeholder === 'cm') this.setValue('height', value);
    else this.setState({ gender: value });
  };

  onClickHandler = () => {
    const { age, weight, height, gender } = this.state;
    this.setState({ saving: true });

    this.props
      .mutate({
        variables: {
          id: this.props.profile.id,
          date: this.props.currentDate,
          age,
          weight,
          height,
          gender,
        },
        refetchQueries: [
          {
            query: profileQuery,
            variables: {
              date: this.props.currentDate,
            },
          },
          {
            query: profilesQuery,
          },
        ],
      })
      .then(() => {
        this.setState({ saving: false });
        this.props.close();
      })
      .catch(err => console.log(err));
  };

  render() {
    const { profile } = this.props;
    const { age, weight, height, gender } = this.state;

    return (
      <Wrapper>
        {this.state.saving
          ? <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          : <div />}

        <Form size="small">
          <label>Age</label>
          <Form.Field
            control={Input}
            placeholder="years"
            onChange={this.onChangeHandler}
            value={this.state.age}
          />

          <label>Weight</label>
          <Form.Field
            control={Input}
            placeholder="kg"
            onChange={this.onChangeHandler}
            value={this.state.weight}
          />

          <label>Height</label>
          <Form.Field
            control={Input}
            placeholder="cm"
            onChange={this.onChangeHandler}
            value={this.state.height}
          />

          <label>Gender</label>
          <Form.Field
            control={Select}
            options={options}
            placeholder="gender"
            onChange={this.onChangeHandler}
            value={this.state.gender}
          />
        </Form>

        <ButtonsWrapper>
          <Button basic color="red" onClick={this.props.close}>
            Cancel
          </Button>
          <Button
            basic
            color="violet"
            onClick={this.onClickHandler}
            disabled={
              (age === profile.age &&
                weight === profile.weight &&
                height === profile.height &&
                gender === profile.gender) ||
              age === '' ||
              weight === '' ||
              height === ''
            }
          >
            Save
          </Button>
        </ButtonsWrapper>
      </Wrapper>
    );
  }
}

export default graphql(mutation)(PersonalData);
