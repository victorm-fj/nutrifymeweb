import React, { Component } from 'react';
import { Button, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import { graphql } from 'react-apollo';

import Footer from '../components/Footer';
import HomeHeader from '../components/HomeHeader';
import currentUserQuery from '../queries/CurrentUser';
import SignInModal from './SignInModal';
import SignUpModal from './SignUpModal';
import Slider from './Slider';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 90%;
  margin: 0 auto;
`;

const Container = styled.div`
  display: flex;
  margin-bottom: 56px;
`;

class Home extends Component {
  state = {
    signInOpened: false,
    signUpOpened: false,
  };

  componentWillUpdate(nextProps) {
    if (!nextProps.data.loading && nextProps.data.user) {
      this.props.history.push('/dashboard');
    }
  }

  openSignInModal = () => {
    this.setState({ signInOpened: true });
  };

  openSignUpModal = () => {
    this.setState({ signUpOpened: true });
  };

  closeSignInModal = () => {
    this.setState({ signInOpened: false });
  };

  closeSignUpModal = () => {
    this.setState({ signUpOpened: false });
  };

  render() {
    if (this.props.data.loading) {
      return (
        <Wrapper>
          <Dimmer active inverted>
            <Loader inverted>Loading</Loader>
          </Dimmer>
        </Wrapper>
      );
    }

    return (
      <Wrapper>
        <HomeHeader />

        <Slider />

        <Container>
          <Button
            basic
            type="submit"
            color="violet"
            size="large"
            onClick={this.openSignInModal}
          >
            Log In
          </Button>

          <Button
            basic
            type="submit"
            color="blue"
            size="large"
            onClick={this.openSignUpModal}
          >
            Register
          </Button>
        </Container>

        <Footer />

        <SignInModal
          open={this.state.signInOpened}
          close={this.closeSignInModal}
          history={this.props.history}
        />

        <SignUpModal
          open={this.state.signUpOpened}
          close={this.closeSignUpModal}
          history={this.props.history}
        />
      </Wrapper>
    );
  }
}

export default graphql(currentUserQuery)(Home);
