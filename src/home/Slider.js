import React, { Component } from 'react';
import { ViewPager, Frame, Track, View } from 'react-view-pager';
import styled from 'styled-components';
import { Icon } from 'semantic-ui-react';

import introData from '../data/intro.json';
import SliderItem from './SliderItem';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 80%;
  position: relative;
`;

const DotBtn = styled.button`
  background: none;
  border: none;

  &:focus {
    outline: 0;
  }
`;

const DotsWrapper = styled.div`
  padding: 10px 0;
  text-align: center;
`;

const NavButtons = styled.nav`
  display: flex;
  justify-content: space-between;
`;

const NavBtn = styled.button`
  position: absolute;
  top: 40%;
  background: none;
  border: none;

  &:focus {
    outline: 0;
  }
`;

const Prev = styled(NavBtn)`
  left: -40px;
`;

const Next = styled(NavBtn)`
  right: -40px;
`;

class Slider extends Component {
  state = { currentView: 0 };

  onViewChangeHandler = currentIndex => {
    this.setState({ currentView: currentIndex[0] });
  };

  render() {
    return (
      <Wrapper>
        <ViewPager tag="main">
          <Frame accessibility={false}>
            <Track
              infinite
              ref={c => (this.track = c)}
              viewsToShow={1}
              currentView={this.state.currentView}
              onViewChange={this.onViewChangeHandler}
            >
              {introData.map(intro => {
                return (
                  <View key={intro.text}>
                    <SliderItem number={intro.id} text={intro.text} />
                  </View>
                );
              })}
            </Track>
          </Frame>

          <DotsWrapper>
            <DotBtn onClick={() => this.track.scrollTo(0)}>
              <Icon
                name="circle"
                size="small"
                color="violet"
                disabled={this.state.currentView !== 0}
              />
            </DotBtn>

            <DotBtn onClick={() => this.track.scrollTo(1)}>
              <Icon
                name="circle"
                size="small"
                color="violet"
                disabled={this.state.currentView !== 1}
              />
            </DotBtn>

            <DotBtn onClick={() => this.track.scrollTo(2)}>
              <Icon
                name="circle"
                size="small"
                color="violet"
                disabled={this.state.currentView !== 2}
              />
            </DotBtn>

            <DotBtn onClick={() => this.track.scrollTo(3)}>
              <Icon
                name="circle"
                size="small"
                color="violet"
                disabled={this.state.currentView !== 3}
              />
            </DotBtn>
          </DotsWrapper>

          <NavButtons>
            <Prev onClick={() => this.track.prev()}>
              <Icon name="arrow left" size="large" color="violet" />
            </Prev>

            <Next onClick={() => this.track.next()}>
              <Icon name="arrow right" size="large" color="violet" />
            </Next>
          </NavButtons>
        </ViewPager>
      </Wrapper>
    );
  }
}

export default Slider;
