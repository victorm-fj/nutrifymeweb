import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import styled from 'styled-components';
import { graphql } from 'react-apollo';

import SignForm from './SignForm';
import mutation from '../mutations/LogIn';
import query from '../queries/CurrentUser';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  position: relative;
  padding: 40px;
`;

const Heading = styled.h3`
  position: absolute;
  top: 15px;
`;

const Social = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  padding-top: 20px;

  @media (max-width: 540px) {
    flex-direction: column;
    justify-content: space-around;
    height: 160px;
    max-width: 220px;
  }
`;

class SignIn extends Component {
  state = { error: '' };

  onLoginHandler = ({ email, password }) => {
    this.props
      .mutate({
        variables: { email, password },
        refetchQueries: [{ query }],
      })
      .catch(err => this.setState({ error: err.graphQLErrors[0].message }));
  };

  clearError = () => {
    this.setState({ error: '' });
  };

  render() {
    return (
      <Wrapper>
        <Heading>Sign in using</Heading>

        <Social>
          <Button
            color="facebook"
            size="large"
            as="a"
            href="http://localhost:8080/login/facebook"
          >
            <Icon name="facebook" /> Facebook
          </Button>

          <Button
            color="twitter"
            size="large"
            as="a"
            href="http://localhost:8080/login/twitter"
          >
            <Icon name="twitter" /> Twitter
          </Button>

          <Button
            color="google plus"
            size="large"
            as="a"
            href="http://localhost:8080/login/google"
          >
            <Icon name="google plus" /> Google
          </Button>
        </Social>

        <h3 style={{ margin: '10px 0' }}>or</h3>

        <SignForm
          btnColor="violet"
          btnText="Log In"
          onSubmit={this.onLoginHandler}
          error={this.state.error}
          onChangeInput={this.clearError}
        />

        <a href="/" style={{ marginTop: '10px' }}>
          Forgot your password?
        </a>
      </Wrapper>
    );
  }
}

export default graphql(mutation)(SignIn);
