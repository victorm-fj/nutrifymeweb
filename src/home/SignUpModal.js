import React from 'react';
import { Icon, Modal } from 'semantic-ui-react';
import styled from 'styled-components';

import SignUp from './SignUp';

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 10px;
  z-index: 10;
`;

const SignUpModal = ({ open, close, history }) =>
  <Modal open={open}>
    <Modal.Content>
      <Button onClick={close}>
        <Icon name="close" size="large" color="blue" />
      </Button>

      <SignUp history={history} />
    </Modal.Content>
  </Modal>;

export default SignUpModal;
