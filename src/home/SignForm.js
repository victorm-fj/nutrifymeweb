import React, { Component } from 'react';
import { Button, Input, Form } from 'semantic-ui-react';
import styled from 'styled-components';

const SRTLabel = styled.label`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
`;

const ErrorText = styled.p`
  font-size: 0.9rem;
  color: #c70039;
  font-weight: bold;
  padding-top: 3px;
`;

// Email validation, returns true if a valid email is received, false otherwise
const validateEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

const validatePassword = password => {
  return password.length >= 6;
};

class SignForm extends Component {
  state = {
    email: '',
    password: '',
    disabled: true,
    emailError: '',
    passwordError: '',
  };

  enableButton = () => {
    const { email, password, emailError, passwordError } = this.state;
    const disabled = !email || !password || !!emailError || !!passwordError;
    this.setState({ disabled });
  };

  onEmailChange = event => {
    this.props.onChangeInput();
    const email = event.target.value;

    if (!email) {
      this.setState({ email, emailError: '' }, this.enableButton);
    } else if (!validateEmail(email)) {
      this.setState(
        { email: '', emailError: 'Please enter a valid email address.' },
        this.enableButton
      );
    } else {
      this.setState({ email, emailError: '' }, this.enableButton);
    }
  };

  onPasswordChange = event => {
    this.props.onChangeInput();
    const password = event.target.value;

    if (!password) {
      this.setState({ password, passwordError: '' }, this.enableButton);
    } else if (!validatePassword(password)) {
      this.setState(
        { passwordError: 'Password must be at least 6 characters.' },
        this.enableButton
      );
    } else {
      this.setState({ password, passwordError: '' }, this.enableButton);
    }
  };

  onClickHandler = event => {
    event.preventDefault();
    const { email, password } = this.state;
    this.props.onSubmit({ email, password });
  };

  render() {
    return (
      <Form size="large">
        <Form.Field>
          <SRTLabel>Email</SRTLabel>
          <Input
            icon="mail"
            iconPosition="left"
            placeholder="Email"
            onChange={this.onEmailChange}
          />
          <ErrorText>
            {this.state.emailError}
          </ErrorText>
        </Form.Field>

        <Form.Field>
          <SRTLabel>Password</SRTLabel>
          <Input
            type="password"
            icon="privacy"
            iconPosition="left"
            placeholder="Password"
            onChange={this.onPasswordChange}
          />
          <ErrorText>
            {this.state.passwordError}
          </ErrorText>
        </Form.Field>

        <ErrorText>
          {this.props.error}
        </ErrorText>

        <Form.Field>
          <Button
            basic
            type="submit"
            color={this.props.btnColor}
            size="large"
            disabled={this.state.disabled}
            onClick={this.onClickHandler}
          >
            {this.props.btnText}
          </Button>
        </Form.Field>
      </Form>
    );
  }
}

export default SignForm;
