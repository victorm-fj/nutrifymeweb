import React, { Component } from 'react';
import styled from 'styled-components';
import { graphql } from 'react-apollo';

import SignForm from './SignForm';
import mutation from '../mutations/SignUp';
import query from '../queries/CurrentUser';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  padding: 70px 40px 40px;
`;

const Heading = styled.h3`
  position: absolute;
  top: 15px;
`;

const Policies = styled.p`
  padding-top: 20px;
  width: 60%;
  text-align: justify;
  color: #757575;

  @media (max-width: 400px) {
    width: 100%;
  }
`;

class SignUp extends Component {
  state = { error: '' };

  onSignupHandler = ({ email, password }) => {
    this.props
      .mutate({
        variables: { email, password },
        refetchQueries: [{ query }],
      })
      .catch(err => this.setState({ error: err.graphQLErrors[0].message }));
  };

  clearError = () => {
    this.setState({ error: '' });
  };

  render() {
    return (
      <Wrapper>
        <Heading>New user? Sign up</Heading>

        <SignForm
          btnColor="blue"
          btnText="Register"
          onSubmit={this.onSignupHandler}
          error={this.state.error}
          onChangeInput={this.clearError}
        />

        <Policies>
          By signing up, you agree to the <a href="/">Terms of Service</a> and{' '}
          <a href="/">Privacy Police</a>, including <a href="/">Cookie Use</a>.
        </Policies>
      </Wrapper>
    );
  }
}

export default graphql(mutation)(SignUp);
