import React from 'react';
import { Icon, Modal } from 'semantic-ui-react';
import styled from 'styled-components';

import SignIn from './SignIn';

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 10px;
  z-index: 10;
`;

const SignInModal = ({ open, close, history }) =>
  <Modal open={open}>
    <Modal.Content>
      <Button onClick={close}>
        <Icon name="close" size="large" color="violet" />
      </Button>

      <SignIn history={history} />
    </Modal.Content>
  </Modal>;

export default SignInModal;
