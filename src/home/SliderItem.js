import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 300px;
`;

const SliderItem = ({ number, text }) =>
  <Wrapper>
    <h1>
      {number}
    </h1>
    <p style={{ textAlign: 'center' }}>
      {text}
    </p>
  </Wrapper>;

export default SliderItem;
