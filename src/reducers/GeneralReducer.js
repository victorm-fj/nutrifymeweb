import moment from 'moment';
import { SET_SEARCH_TERM, SET_ACTIVE_MEAL, SET_DATE } from '../actions/types';

const initialState = {
  searchTerm: '',
  activeMeal: 'breakfast',
  date: moment().format('YYYY-MM-DD'),
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_SEARCH_TERM: {
      return { ...state, ...payload };
    }

    case SET_ACTIVE_MEAL: {
      return { ...state, ...payload };
    }

    case SET_DATE: {
      return { ...state, ...payload };
    }

    default:
      return state;
  }
};
