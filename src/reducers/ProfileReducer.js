import moment from 'moment';
import { CREATE_PROFILE } from '../actions/types';

const initialState = {
  unitSystem: 0,
  activityLevel: 1,
  age: 0,
  weight: 0,
  height: 0,
  gender: 'male',
  date: moment().format('YYYY-MM-DD'),
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_PROFILE: {
      return { ...state, ...payload };
    }

    default:
      return state;
  }
};
