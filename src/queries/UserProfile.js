import { gql } from 'react-apollo';

export default gql`
  query Profile($date: String!) {
    profile(date: $date) {
      id
      date
      age
      weight
      height
      gender
      activityLevel
      goal
      calRestriction
      needs {
        tee
        carbs
        prots
        fats
      }
      results {
        bmi
        interval
        legend
        idealBW
        minBW
        maxBW
      }
    }
  }
`;
