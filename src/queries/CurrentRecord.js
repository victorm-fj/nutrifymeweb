import { gql } from 'react-apollo';

export default gql`
  query Record($profileId: String, $date: String) {
    record(profileId: $profileId, date: $date) {
      id
      date
      profileId
      foods {
        id
        meal
        quantity
        units
        food {
          id
          description
          kcal
          protein_g
          lipid_total_g
          carbohydrate_g
          fiber_td_g
          fa_sat_g
          fa_mono_g
          fa_poly_g
          gmwt_1
          gmwt_desc1
          gmwt_2
          gmwt_desc2
        }
      }
    }
  }
`;
