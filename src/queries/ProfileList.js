import { gql } from 'react-apollo';

export default gql`
  {
    profiles {
      id
      weight
      date
      results {
        minBW
        maxBW
      }
    }
  }
`;
