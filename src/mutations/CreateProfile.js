import { gql } from 'react-apollo';

export default gql`
  mutation CreateProfile(
    $date: String
    $unitSystem: Int
    $age: Int
    $weight: Float
    $height: Float
    $gender: String
    $activityLevel: Int
    $goal: Int
    $calRestriction: Int
  ) {
    createProfile(
      date: $date
      unitSystem: $unitSystem
      age: $age
      weight: $weight
      height: $height
      gender: $gender
      activityLevel: $activityLevel
      goal: $goal
      calRestriction: $calRestriction
    ) {
      id
    }
  }
`;
