import { gql } from 'react-apollo';

export default gql`
  mutation DeleteFood($id: ID!, $recordId: ID!) {
    deleteFood(id: $id, recordId: $recordId) {
      id
      date
      profileId
    }
  }
`;
