import { gql } from 'react-apollo';

export default gql`
  mutation UpdateRecord(
    $recordId: String
    $meal: String
    $foodId: String
    $quantity: String
    $units: String
  ) {
    updateRecord(
      recordId: $recordId
      meal: $meal
      foodId: $foodId
      quantity: $quantity
      units: $units
    ) {
      id
      date
      profileId
    }
  }
`;
