import { gql } from 'react-apollo';

export default gql`
  mutation UpdateProfile(
    $id: ID!
    $date: String!
    $age: Int
    $weight: Float
    $height: Float
    $gender: String
    $activityLevel: Int
    $goal: Int
    $calRestriction: Int
  ) {
    updateProfile(
      id: $id
      date: $date
      age: $age
      weight: $weight
      height: $height
      gender: $gender
      activityLevel: $activityLevel
      goal: $goal
      calRestriction: $calRestriction
    ) {
      id
      date
    }
  }
`;
