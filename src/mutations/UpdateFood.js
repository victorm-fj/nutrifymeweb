import { gql } from 'react-apollo';

export default gql`
  mutation UpdateFood($id: ID!, $quantity: String!, $recordId: ID!) {
    updateFood(id: $id, recordId: $recordId, quantity: $quantity) {
      id
      date
      profileId
    }
  }
`;
