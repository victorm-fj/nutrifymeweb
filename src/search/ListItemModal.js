import React from 'react';
import { Modal, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

import ItemDetails from './ItemDetails';

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 0px;
  top: 8px;
`;

const ListItemModal = ({
  open,
  close,
  food,
  history,
  recordId,
  activeMeal,
  profileId,
  currentDate,
}) => {
  return (
    <Modal
      open={open}
      dimmer={false}
      style={{
        width: '80%',
        marginTop: '-240px',
        marginLeft: '-40%',
        paddingTop: '40px',
      }}
    >
      <Modal.Content>
        <Button onClick={close}>
          <Icon name="close" size="large" color="violet" />
        </Button>

        <ItemDetails
          close={close}
          food={food}
          history={history}
          recordId={recordId}
          activeMeal={activeMeal}
          profileId={profileId}
          currentDate={currentDate}
        />
      </Modal.Content>
    </Modal>
  );
};

export default ListItemModal;
