import React, { Component } from 'react';
import { Form, Input, Dimmer, Loader, Dropdown } from 'semantic-ui-react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';

import { setSearchTerm, setActiveMeal } from '../actions';
import query from '../queries/FoodList';
import ListComponent from './ListComponent';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  min-height: 100vh;
  background-color: #fff;
  padding-top: 20px;
`;

const SRTLabel = styled.label`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
`;

const options = [
  { key: 'b', text: 'Breakfast', value: 'breakfast' },
  { key: 'm', text: 'Mid-morning snack', value: 'midMorning' },
  { key: 'l', text: 'Lunch', value: 'lunch' },
  { key: 'a', text: 'Afternoon snack', value: 'afternoonSnack' },
  { key: 'd', text: 'Dinner', value: 'dinner' },
];

class SearchContainer extends Component {
  state = { searchTerm: '', currentMeal: this.props.activeMeal };

  onChangeHandler = event => {
    this.setState({ searchTerm: event.target.value });
  };

  onChangeMealHandler = (event, data) => {
    this.props.setActiveMeal(data.value);
  };

  onSubmitHandler = e => {
    e.preventDefault();
    this.props.setSearchTerm(this.state.searchTerm);
  };

  renderFoodList() {
    const {
      data,
      history,
      location,
      profileQuery,
      activeMeal,
      currentDate,
    } = this.props;
    if (data) {
      if (data.loading) {
        return (
          <Dimmer active inverted>
            <Loader inverted>Loading</Loader>
          </Dimmer>
        );
      }

      return (
        <ListComponent
          foods={data.foods}
          history={history}
          recordId={location.state.recordId}
          activeMeal={activeMeal}
          profileId={profileQuery.profile.id}
          currentDate={currentDate}
        />
      );
    }

    return <div />;
  }

  render() {
    return (
      <Wrapper>
        <Dropdown
          inline
          options={options}
          onChange={this.onChangeMealHandler}
          defaultValue={this.state.currentMeal}
          style={{ marginBottom: '5px' }}
        />

        <Form size="large" onSubmit={this.onSubmitHandler}>
          <SRTLabel>Search box</SRTLabel>
          <Form.Field
            control={Input}
            placeholder="Search foods..."
            onChange={this.onChangeHandler}
          />
        </Form>

        {this.renderFoodList()}
      </Wrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    description: state.general.searchTerm,
    activeMeal: state.general.activeMeal,
    currentDate: state.general.date,
  };
};

export default compose(
  connect(mapStateToProps, { setSearchTerm, setActiveMeal }),
  graphql(query, {
    skip: ownProps => ownProps.description === '',
    options: ownProps => ({
      variables: { description: ownProps.description },
    }),
  })
)(SearchContainer);
