import React, { Component } from 'react';
import { Button, Form, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import { graphql } from 'react-apollo';

import query from '../queries/CurrentRecord';
import mutation from '../mutations/UpdateRecord';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  margin: 0 auto;
`;

const Title = styled.h3`
  color: #212121;
  font-size: 1.15rem;
  margin-bottom: 15px;
`;

const SubTitle = styled.h4`
  color: #757575;
  font-size: 1rem;
  margin: 10px 0 5px;
`;

const PrimaryText = styled.p`
  color: #757575;
  font-size: 0.9rem;
  margin: 0 0 5px;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
  margin-bottom: 20px;
`;

const ErrorText = styled.p`
  font-size: 0.9rem;
  color: #c70039;
  font-weight: bold;
  padding-top: 3px;
  text-align: center;
`;

class ItemDetails extends Component {
  state = { selectValue: 'g', value: '100', error: '', saving: false };

  onChangeSelectHandler = (event, data) => {
    data.value === 'g'
      ? this.setState({ selectValue: data.value, value: '100' })
      : this.setState({ selectValue: data.value, value: '1' });
  };

  onChangeInputHandler = (event, data) => {
    Number(data.value) >= 0
      ? this.setState({ value: data.value, error: '' })
      : this.setState({ error: 'You must enter a number' });
  };

  onClickHandler = () => {
    this.setState({ saving: true });
    this.props
      .mutate({
        variables: {
          recordId: this.props.recordId,
          meal: this.props.activeMeal,
          foodId: this.props.food.id,
          quantity: this.state.value,
          units: this.state.selectValue,
        },
        refetchQueries: [
          {
            query,
            variables: {
              profileId: this.props.profileId,
              date: this.props.currentDate,
            },
          },
        ],
      })
      .then(() => {
        this.setState({ saving: false });
        this.props.close();
        this.props.history.push('/dashboard/home');
      })
      .catch(err => console.log(err));
  };

  calculateValue = nutrient => {
    const { gmwt_1, gmwt_desc1, gmwt_2, gmwt_desc2 } = this.props.food;
    const { selectValue, value } = this.state;

    if (selectValue === gmwt_desc1) {
      return Math.round(nutrient * Number(value) * gmwt_1 / 100 * 100) / 100;
    } else if (selectValue === gmwt_desc2) {
      return Math.round(nutrient * Number(value) * gmwt_2 / 100 * 100) / 100;
    } else {
      return Math.round(nutrient * Number(value) / 100 * 100) / 100;
    }
  };

  render() {
    const {
      description,
      kcal,
      carbohydrate_g,
      protein_g,
      lipid_total_g,
      gmwt_desc1,
      gmwt_desc2,
    } = this.props.food;

    const options = [
      { key: 'g', text: 'g', value: 'g' },
      { key: gmwt_desc1, text: gmwt_desc1, value: gmwt_desc1 },
      { key: gmwt_desc2, text: gmwt_desc2, value: gmwt_desc2 },
    ];

    return (
      <Wrapper>
        {this.state.saving
          ? <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          : <div />}

        <Title>
          {description}
        </Title>

        <Form size="tiny">
          <Form.Group width="equals">
            <Form.Input
              label="Portion"
              value={this.state.value}
              onChange={this.onChangeInputHandler}
            />

            <Form.Select
              label="Units"
              options={options}
              onChange={this.onChangeSelectHandler}
              defaultValue="g"
            />
          </Form.Group>

          <ErrorText>
            {this.state.error}
          </ErrorText>
        </Form>

        <SubTitle>Nutritional Information</SubTitle>
        <PrimaryText>
          Energy: {this.calculateValue(kcal)} kcal
        </PrimaryText>
        <PrimaryText>
          Carbohydrates: {this.calculateValue(carbohydrate_g)} g
        </PrimaryText>
        <PrimaryText>
          Proteins: {this.calculateValue(protein_g)} g
        </PrimaryText>
        <PrimaryText>
          Total Fats: {this.calculateValue(lipid_total_g)} g
        </PrimaryText>

        <ButtonsWrapper>
          <Button basic color="red" onClick={this.props.close}>
            Cancel
          </Button>
          <Button
            basic
            color="violet"
            onClick={this.onClickHandler}
            disabled={this.state.value === '' || Number(this.state.value) === 0}
          >
            Save
          </Button>
        </ButtonsWrapper>
      </Wrapper>
    );
  }
}

export default graphql(mutation)(ItemDetails);
