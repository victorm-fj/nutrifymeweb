import React, { Component } from 'react';
import { List } from 'semantic-ui-react';
import styled from 'styled-components';

import ListItemModal from './ListItemModal';

const Wrapper = styled.div`padding: 5px;`;

class ListItem extends Component {
  state = {
    open: false,
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      description,
      kcal,
      carbohydrate_g,
      protein_g,
      lipid_total_g,
    } = this.props.food;

    return (
      <List.Item onClick={() => this.setState({ open: true })}>
        <Wrapper>
          <List.Content>
            <List.Header>
              {description}
            </List.Header>
            <List.Description className="food-details">
              kcal {kcal}, carbs: {carbohydrate_g}, prots: {protein_g}, fats:{' '}
              {lipid_total_g}
            </List.Description>
          </List.Content>
        </Wrapper>

        <ListItemModal
          open={this.state.open}
          close={this.closeModal}
          food={this.props.food}
          history={this.props.history}
          recordId={this.props.recordId}
          activeMeal={this.props.activeMeal}
          profileId={this.props.profileId}
          currentDate={this.props.currentDate}
        />
      </List.Item>
    );
  }
}

export default ListItem;
