import React from 'react';
import { List } from 'semantic-ui-react';
import styled from 'styled-components';

import ListItem from './ListItem';

const Wrapper = styled.div`
  width: 100%;
  margin: 20px 0;
`;

const ListComponent = ({
  foods,
  history,
  recordId,
  activeMeal,
  profileId,
  currentDate,
}) => {
  return (
    <Wrapper>
      <List divided animated>
        {foods.map(food =>
          <ListItem
            key={food.id}
            food={food}
            history={history}
            recordId={recordId}
            activeMeal={activeMeal}
            profileId={profileId}
            currentDate={currentDate}
          />
        )}
      </List>
    </Wrapper>
  );
};

export default ListComponent;
