import { SET_SEARCH_TERM, SET_ACTIVE_MEAL, SET_DATE } from './types';

export const setSearchTerm = searchTerm => {
  return {
    type: SET_SEARCH_TERM,
    payload: { searchTerm },
  };
};

export const setActiveMeal = activeMeal => {
  return {
    type: SET_ACTIVE_MEAL,
    payload: { activeMeal },
  };
};

export const setDate = date => {
  return {
    type: SET_DATE,
    payload: { date },
  };
};
