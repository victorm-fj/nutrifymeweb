import {
  CREATE_PROFILE,
} from './types';

export const createProfile = (userData) => {
  return {
    type: CREATE_PROFILE,
    payload: userData,
  };
};
