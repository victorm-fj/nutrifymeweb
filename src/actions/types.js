export const CREATE_PROFILE = 'create_profile';
export const SET_SEARCH_TERM = 'set_search_term';
export const SET_ACTIVE_MEAL = 'set_active_meal';
export const SET_DATE = 'set_date';
