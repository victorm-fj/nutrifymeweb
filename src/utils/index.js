export const calculateTotalValues = foods => {
  let totalKcal = 0,
    totalCarbs = 0,
    totalProts = 0,
    totalFats = 0;

  foods.forEach(food => {
    const {
      quantity,
      units,
      food: {
        kcal,
        carbohydrate_g,
        protein_g,
        lipid_total_g,
        gmwt_1,
        gmwt_desc1,
        gmwt_2,
        gmwt_desc2,
      },
    } = food;

    if (units === gmwt_desc1) {
      totalKcal += kcal * Number(quantity) * gmwt_1 / 100;
      totalCarbs += carbohydrate_g * Number(quantity) * gmwt_1 / 100;
      totalProts += protein_g * Number(quantity) * gmwt_1 / 100;
      totalFats += lipid_total_g * Number(quantity) * gmwt_1 / 100;
    } else if (units === gmwt_desc2) {
      totalKcal += kcal * Number(quantity) * gmwt_2 / 100;
      totalCarbs += carbohydrate_g * Number(quantity) * gmwt_2 / 100;
      totalProts += protein_g * Number(quantity) * gmwt_2 / 100;
      totalFats += lipid_total_g * Number(quantity) * gmwt_2 / 100;
    } else {
      totalKcal += kcal * Number(quantity) / 100;
      totalCarbs += carbohydrate_g * Number(quantity) / 100;
      totalProts += protein_g * Number(quantity) / 100;
      totalFats += lipid_total_g * Number(quantity) / 100;
    }
  });

  totalKcal = Math.round(totalKcal * 100) / 100;
  totalCarbs = Math.round(totalCarbs * 100) / 100;
  totalProts = Math.round(totalProts * 100) / 100;
  totalFats = Math.round(totalFats * 100) / 100;

  return { totalKcal, totalCarbs, totalProts, totalFats };
};

export const calculateCalories = (recommendedKcal, totalKcal) => {
  const eatenKcal = Math.round(totalKcal);
  const remainingKcal = Math.round(recommendedKcal - eatenKcal);
  const percentageEaten = Math.round(eatenKcal / recommendedKcal * 100);
  const percentageRemaining = 100 - percentageEaten;

  return { eatenKcal, remainingKcal, percentageEaten, percentageRemaining };
};

export const calculateMacrosPercentage = (needs, foods) => {
  const { tee, carbs, prots, fats } = needs;
  const { totalKcal, totalCarbs, totalProts, totalFats } = calculateTotalValues(
    foods
  );

  const totalKcalPercentage = Math.round(totalKcal / tee * 100);
  const totalCarbsPercentage = Math.round(totalCarbs / carbs * 100);
  const totalProtsPercentage = Math.round(totalProts / prots * 100);
  const totalFatsPercentage = Math.round(totalFats / fats * 100);

  return {
    totalKcalPercentage,
    totalCarbsPercentage,
    totalProtsPercentage,
    totalFatsPercentage,
  };
};

export const calculateMacrosResume = (needs, foods) => {
  const { tee, carbs, prots, fats } = needs;
  const { totalKcal, totalCarbs, totalProts, totalFats } = calculateTotalValues(
    foods
  );

  const remainingKcal = Math.round(tee - totalKcal);
  const remainingCarbs = Math.round(carbs - totalCarbs);
  const remainingProts = Math.round(prots - totalProts);
  const remainingFats = Math.round(fats - totalFats);

  return {
    kcal: { kcal: tee, eatenKcal: Math.round(totalKcal), remainingKcal },
    carbs: { carbs, eatenCarbs: Math.round(totalCarbs), remainingCarbs },
    prots: { prots, eatenProts: Math.round(totalProts), remainingProts },
    fats: { fats, eatenFats: Math.round(totalFats), remainingFats },
  };
};

export const getDefaultActiveIndex = activeMeal => {
  switch (activeMeal) {
    case 'breakfast':
      return 0;
    case 'midMorning':
      return 1;
    case 'lunch':
      return 2;
    case 'afternoonSnack':
      return 3;
    case 'dinner':
      return 4;
    default:
      return 0;
  }
};
