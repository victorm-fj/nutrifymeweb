import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Responsive from 'react-responsive';
import styled from 'styled-components';
import {
  ApolloClient,
  ApolloProvider,
  createNetworkInterface,
} from 'react-apollo';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';

import Hamburger from './components/Hamburger';
import About from './components/About';
import TermsOfUse from './components/TermsOfUse';
import PrivacyPolice from './components/PrivacyPolice';
import CookieUse from './components/CookieUse';
import Home from './home';
import requireAuth from './components/requireAuth';
import Dashboard from './dashboard';
import Search from './search';
import profile from './reducers/ProfileReducer';
import general from './reducers/GeneralReducer';

const Wrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  align-items: center;
  position: relative;
`;

const URI_ENDPOINT =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost:8080/'
    : 'https://jx68totn7e.execute-api.us-east-1.amazonaws.com/dev/';

const networkInterface = createNetworkInterface({
  uri: URI_ENDPOINT,
  opts: {
    credentials: 'include', // fetch cookies on every query req
  },
});

const client = new ApolloClient({
  networkInterface,
  dataIdFromObject: o => o.id,
});

const store = createStore(
  combineReducers({
    apollo: client.reducer(),
    profile,
    general,
  }),
  {}, // initial state
  compose(
    applyMiddleware(client.middleware()),
    // using devToolsExtension
    typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined'
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : f => f
  )
);

class App extends Component {
  render() {
    return (
      <ApolloProvider store={store} client={client}>
        <BrowserRouter>
          <Wrapper>
            <Responsive maxWidth={550}>
              <Hamburger />
            </Responsive>

            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/about" component={About} />
              <Route path="/terms" component={TermsOfUse} />
              <Route path="/privacy" component={PrivacyPolice} />
              <Route path="/cookie" component={CookieUse} />
              <Route path="/dashboard" component={requireAuth(Dashboard)} />
              <Route path="/search" component={requireAuth(Search)} />
            </Switch>
          </Wrapper>
        </BrowserRouter>
      </ApolloProvider>
    );
  }
}

export default App;
