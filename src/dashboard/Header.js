import React, { Component } from 'react';
import styled from 'styled-components';
import { Button, Dimmer, Loader } from 'semantic-ui-react';
import { ApolloClient, withApollo, graphql } from 'react-apollo';
import PropTypes from 'prop-types';

import logo from '../logo.png';
import mutation from '../mutations/LogOut';

const Wrapper = styled.div`
  width: 100%;
  height: 56px;
  padding: 16px;
  display: flex;
  align-items: center;
  position: relative;
  box-shadow: 0px 0px 10px 0px rgba(212, 212, 213, 1);
`;

const LoaderWrapper = styled.div`
  display: flex;
  min-height: 100vh;
  width: 100%;
  flex-direction: column;
  align-items: center;
  position: relative;
`;

const Logo = styled.img.attrs({
  src: logo,
  alt: 'NutrifyMe logo',
})`
  height: 30px;
  margin-right: 10px;
`;

const Brand = styled.h1`
  letter-spacing: 0.1rem;
  color: #189963;
`;

const LogoutButton = styled(Button).attrs({
  basic: true,
  color: 'violet',
})`
  position: absolute;
  right: 20px;
  top: 12px;
`;

class Header extends Component {
  state = { loggingOut: false };

  onLogoutHandler = () => {
    this.setState({ loggingOut: true });
    this.props
      .mutate()
      .then(() => this.props.client.resetStore())
      .catch(err => console.error(err));
  };

  render() {
    if (this.state.loggingOut) {
      return (
        <LoaderWrapper>
          <Dimmer active inverted>
            <Loader inverted>Loading</Loader>
          </Dimmer>
        </LoaderWrapper>
      );
    }

    return (
      <Wrapper>
        <Logo />
        <Brand>NutrifyMe</Brand>
        <LogoutButton onClick={this.onLogoutHandler}>Log out</LogoutButton>
      </Wrapper>
    );
  }
}

Header.propTypes = { client: PropTypes.instanceOf(ApolloClient) };

export default withApollo(graphql(mutation)(Header));
