import React from 'react';
import styled from 'styled-components';

import RecommendedMacros from './RecommendedMacros';
import MealsComponent from './MealsComponent';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width: 90%;
  margin: 0 auto 66px;

  @media (min-width: 800px) {
    width: 80%;
  }
`;

const Home = ({ record, profile, history, currentDate, activeMeal }) => {
  if (!record) return <div />;
  return (
    <Wrapper>
      <RecommendedMacros record={record} needs={profile.needs} />

      <MealsComponent
        record={record}
        history={history}
        profileId={profile.id}
        currentDate={currentDate}
        activeMeal={activeMeal}
      />
    </Wrapper>
  );
};

export default Home;
