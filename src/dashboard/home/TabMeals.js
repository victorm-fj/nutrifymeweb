import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { setActiveMeal } from '../../actions';
import { getDefaultActiveIndex } from '../../utils';
import MealItem from './MealItem';

const Wrapper = styled.div`
  flex: 1;
  width: 100%;
  margin: 16px auto 0;
`;

class TabMeals extends Component {
  updatePanes = () => {
    let breakfast = [],
      midMorning = [],
      lunch = [],
      afternoonSnack = [],
      dinner = [];

    this.props.record.foods.forEach(food => {
      if (food.meal === 'breakfast') breakfast.push(food);
      else if (food.meal === 'midMorning') midMorning.push(food);
      else if (food.meal === 'lunch') lunch.push(food);
      else if (food.meal === 'afternoonSnack') afternoonSnack.push(food);
      else dinner.push(food);
    });

    const panes = [
      {
        menuItem: '1',
        meal: 'breakfast',
        render: () =>
          <MealItem
            meal="Breakfast"
            foods={breakfast}
            recordId={this.props.record.id}
            profileId={this.props.profileId}
            currentDate={this.props.currentDate}
          />,
      },
      {
        menuItem: '2',
        meal: 'midMorning',
        render: () =>
          <MealItem
            meal="Mid-morning snack"
            foods={midMorning}
            recordId={this.props.record.id}
            profileId={this.props.profileId}
            currentDate={this.props.currentDate}
          />,
      },
      {
        menuItem: '3',
        meal: 'lunch',
        render: () =>
          <MealItem
            meal="Lunch"
            foods={lunch}
            recordId={this.props.record.id}
            profileId={this.props.profileId}
            currentDate={this.props.currentDate}
          />,
      },
      {
        menuItem: '4',
        meal: 'afternoonSnack',
        render: () =>
          <MealItem
            meal="Afternoon snack"
            foods={afternoonSnack}
            recordId={this.props.record.id}
            profileId={this.props.profileId}
            currentDate={this.props.currentDate}
          />,
      },
      {
        menuItem: '5',
        meal: 'dinner',
        render: () =>
          <MealItem
            meal="Dinner"
            foods={dinner}
            recordId={this.props.record.id}
            profileId={this.props.profileId}
            currentDate={this.props.currentDate}
          />,
      },
    ];

    return panes;
  };

  onTabChangeHandler = (event, data) => {
    const { activeIndex, panes } = data;
    this.props.setActiveMeal(panes[activeIndex].meal);
  };

  render() {
    const panes = this.updatePanes();
    const activeIndex = getDefaultActiveIndex(this.props.activeMeal);

    return (
      <Wrapper>
        <Tab
          panes={panes}
          onTabChange={this.onTabChangeHandler}
          defaultActiveIndex={activeIndex}
        />
      </Wrapper>
    );
  }
}

export default connect(null, { setActiveMeal })(TabMeals);
