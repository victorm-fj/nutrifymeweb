import React, { Component } from 'react';
import { VictoryPie } from 'victory';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 33.33%;
`;

class MacroChart extends Component {
  calculateRemainingValue = () => {
    const { macro, eatenMacro } = this.props;
    return macro - eatenMacro;
  };

  render() {
    const remainingMacro = this.calculateRemainingValue();
    return (
      <Wrapper>
        <VictoryPie
          labels={d => {}}
          colorScale={[this.props.color, '#d4d4d5']}
          padAngle={3}
          data={[
            { x: 1, y: this.props.eatenMacro },
            { x: 2, y: remainingMacro },
          ]}
        />
      </Wrapper>
    );
  }
}

export default MacroChart;
