import React from 'react';
import { Card, Button } from 'semantic-ui-react';
import styled from 'styled-components';

import TabMeals from './TabMeals';

const Wrapper = styled.div`
  flex: 1;
  margin: 15px 0;
  width: 100%;
`;

const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const MealsComponent = ({
  history,
  record,
  profileId,
  currentDate,
  activeMeal,
}) => {
  return (
    <Wrapper>
      <Card fluid={true}>
        <Card.Content>
          <Card.Header>
            <HeaderWrapper>
              <h3>Meals</h3>

              <Button
                basic
                color="violet"
                onClick={() => history.push('/search', { recordId: record.id })}
              >
                Add food
              </Button>
            </HeaderWrapper>
          </Card.Header>
        </Card.Content>

        <Card.Content>
          {!record
            ? <div />
            : <TabMeals
                record={record}
                profileId={profileId}
                currentDate={currentDate}
                activeMeal={activeMeal}
              />}
        </Card.Content>
      </Card>
    </Wrapper>
  );
};

export default MealsComponent;
