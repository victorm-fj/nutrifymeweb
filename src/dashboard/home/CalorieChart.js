import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { VictoryTooltip, VictoryLabel, VictoryPie } from 'victory';
import styled from 'styled-components';

import { calculateCalories } from '../../utils';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 200px;
`;

class CustomLabel extends React.Component {
  static defaultEvents = VictoryTooltip.defaultEvents;
  static propTypes = { text: PropTypes.string };

  render() {
    const { index, data } = this.props;
    const text = `${data[index].text}\n${data[index].value} kcal`;

    return (
      <g>
        <VictoryLabel {...this.props} />
        <VictoryTooltip
          {...this.props}
          x={0}
          y={80}
          text={`${text}`}
          orientation="top"
          pointerLength={0}
          cornerRadius={80}
          width={160}
          height={160}
          style={{ fontSize: 24, fill: 'black' }}
          flyoutStyle={{ fill: 'white' }}
        />
      </g>
    );
  }
}

class CalorieChart extends Component {
  render() {
    const { recommendedKcal, totalKcal } = this.props;

    const {
      eatenKcal,
      remainingKcal,
      percentageEaten,
      percentageRemaining,
    } = calculateCalories(recommendedKcal, totalKcal);

    return (
      <Wrapper>
        <VictoryPie
          style={{
            labels: {
              fontSize: 24,
              fill: 'white',
            },
          }}
          innerRadius={90}
          labelRadius={100}
          labels={d => d.label}
          labelComponent={<CustomLabel />}
          colorScale={['#6435C9', '#d4d4d5']}
          padAngle={3}
          data={[
            {
              x: 'Eaten',
              y: eatenKcal,
              label: `${percentageEaten}%`,
              value: eatenKcal,
              text: 'Eaten',
            },
            {
              x: 'Remaining',
              y: remainingKcal,
              label: `${percentageRemaining}%`,
              value: remainingKcal,
              text: 'Remaining',
            },
          ]}
        />
      </Wrapper>
    );
  }
}

export default CalorieChart;
