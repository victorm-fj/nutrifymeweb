import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { connect } from 'react-redux';

import FoodItemModal from './FoodItemModal';

class FoodItem extends Component {
  state = { open: false };

  calculateValue() {
    const {
      food: { kcal, gmwt_1, gmwt_desc1, gmwt_2, gmwt_desc2 },
      quantity,
      units,
    } = this.props.food;

    if (units === gmwt_desc1)
      return Math.round(kcal * Number(quantity) * gmwt_1 / 100 * 100) / 100;
    else if (units === gmwt_desc2)
      return Math.round(kcal * Number(quantity) * gmwt_2 / 100 * 100) / 100;
    else return Math.round(kcal * Number(quantity) / 100 * 100) / 100;
  }

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    const { food: { description }, quantity, units } = this.props.food;
    return (
      <Table.Row onClick={() => this.setState({ open: true })}>
        <Table.Cell>
          {description}
        </Table.Cell>
        <Table.Cell textAlign="center">
          {quantity}
        </Table.Cell>
        <Table.Cell textAlign="center">
          {units}
        </Table.Cell>
        <Table.Cell textAlign="center">
          {this.calculateValue()}
        </Table.Cell>

        <FoodItemModal
          open={this.state.open}
          close={this.closeModal}
          food={this.props.food}
          recordId={this.props.recordId}
          activeMeal={this.props.activeMeal}
          profileId={this.props.profileId}
          currentDate={this.props.currentDate}
        />
      </Table.Row>
    );
  }
}

const mapStateToProps = state => {
  return { activeMeal: state.general.activeMeal };
};

export default connect(mapStateToProps)(FoodItem);
