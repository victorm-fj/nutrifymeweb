import React from 'react';
import { Card } from 'semantic-ui-react';
import styled from 'styled-components';

import CalorieChart from './CalorieChart';
import MacrosComponent from './MacrosComponent';
import { calculateTotalValues } from '../../utils';

const Wrapper = styled.div`
  flex: 1;
  margin: 15px 0;
  width: 100%;
`;

const MacrosWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const RecommendedMacros = ({
  needs: { tee, carbs, prots, fats },
  record: { foods },
}) => {
  const { totalKcal, totalCarbs, totalProts, totalFats } = calculateTotalValues(
    foods
  );

  return (
    <Wrapper>
      <Card fluid={true}>
        <Card.Content>
          <Card.Header>
            <h3>Recommendations</h3>

            <p>
              {tee} kcal
            </p>
          </Card.Header>
        </Card.Content>

        <Card.Content>
          <CalorieChart recommendedKcal={tee} totalKcal={totalKcal} />

          <MacrosWrapper>
            <MacrosComponent
              label="Carbs"
              macro={carbs}
              eatenMacro={totalCarbs}
              color="#22dd8f"
            />
            <MacrosComponent
              label="Proteins"
              macro={prots}
              eatenMacro={totalProts}
              color="#5d72d3"
            />
            <MacrosComponent
              label="Fats"
              macro={fats}
              color="#ffcc33"
              eatenMacro={totalFats}
            />
          </MacrosWrapper>
        </Card.Content>
      </Card>
    </Wrapper>
  );
};

export default RecommendedMacros;
