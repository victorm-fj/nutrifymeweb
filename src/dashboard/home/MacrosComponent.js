import React from 'react';
import styled from 'styled-components';

import MacroChart from './MacroChart';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const MacrosComponent = ({ label, macro, color, eatenMacro }) => {
  return (
    <Wrapper>
      <p>
        {label}
      </p>

      <MacroChart color={color} macro={macro} eatenMacro={eatenMacro} />
    </Wrapper>
  );
};

export default MacrosComponent;
