import React, { Component } from 'react';
import { Button, Form, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import { graphql, compose } from 'react-apollo';

import query from '../../queries/CurrentRecord';
import updateFoodMutation from '../../mutations/UpdateFood';
import deleteFoodMutation from '../../mutations/DeleteFood';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 90%;
  margin: 0 auto;
`;

const Title = styled.h3`
  color: #212121;
  font-size: 1.15rem;
  margin-bottom: 15px;
`;

const SubTitle = styled.h4`
  color: #757575;
  font-size: 1rem;
  margin: 10px 0 5px;
`;

const PrimaryText = styled.p`
  color: #757575;
  font-size: 0.9rem;
  margin: 0 0 5px;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
  margin-bottom: 20px;
`;

const ErrorText = styled.p`
  font-size: 0.9rem;
  color: #c70039;
  font-weight: bold;
  padding-top: 3px;
  text-align: center;
`;

class FoodItemDetails extends Component {
  state = {
    selectValue: this.props.food.units,
    value: this.props.food.quantity,
    error: '',
    saving: false,
  };

  onChangeSelectHandler = (event, data) => {
    this.setState({ selectValue: data.value });
  };

  onChangeInputHandler = (event, data) => {
    if (data.value === '.') data.value = '0.';
    Number(data.value) >= 0
      ? this.setState({ value: data.value, error: '' })
      : this.setState({ error: 'You must enter a number' });
  };

  onClickDeleteHandler = () => {
    this.setState({ saving: true });
    this.props
      .deleteFood({
        variables: {
          id: this.props.food.id,
          recordId: this.props.recordId,
        },
        refetchQueries: [
          {
            query,
            variables: {
              profileId: this.props.profileId,
              date: this.props.currentDate,
            },
          },
        ],
      })
      .then(() => {
        this.setState({ saving: false });
        this.props.close();
      })
      .catch(err => console.log(err));
  };

  onClickSaveHandler = () => {
    this.setState({ saving: true });
    this.props
      .updateFood({
        variables: {
          id: this.props.food.id,
          quantity: this.state.value,
          recordId: this.props.recordId,
        },
        refetchQueries: [
          {
            query,
            variables: {
              profileId: this.props.profileId,
              date: this.props.currentDate,
            },
          },
        ],
      })
      .then(() => {
        this.setState({ saving: false });
        this.props.close();
      })
      .catch(err => console.log(err));
  };

  calculateValue = nutrient => {
    const {
      food: { gmwt_1, gmwt_desc1, gmwt_2, gmwt_desc2 },
    } = this.props.food;
    const { selectValue, value } = this.state;

    if (selectValue === gmwt_desc1) {
      return Math.round(nutrient * Number(value) * gmwt_1 / 100 * 100) / 100;
    } else if (selectValue === gmwt_desc2) {
      return Math.round(nutrient * Number(value) * gmwt_2 / 100 * 100) / 100;
    } else {
      return Math.round(nutrient * Number(value) / 100 * 100) / 100;
    }
  };

  render() {
    const {
      food: { description, kcal, carbohydrate_g, protein_g, lipid_total_g },
      units,
    } = this.props.food;

    const options = [{ key: units, text: units, value: units }];

    return (
      <Wrapper>
        {this.state.saving
          ? <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          : <div />}

        <Title>
          {description}
        </Title>

        <Form size="tiny">
          <Form.Group width="equals">
            <Form.Input
              label="Portion"
              value={this.state.value}
              onChange={this.onChangeInputHandler}
            />

            <Form.Select
              label="Units"
              options={options}
              onChange={this.onChangeSelectHandler}
              defaultValue={this.state.selectValue}
            />
          </Form.Group>

          <ErrorText>
            {this.state.error}
          </ErrorText>
        </Form>

        <SubTitle>Nutritional Information</SubTitle>
        <PrimaryText>
          Energy: {this.calculateValue(kcal)} kcal
        </PrimaryText>
        <PrimaryText>
          Carbohydrates: {this.calculateValue(carbohydrate_g)} g
        </PrimaryText>
        <PrimaryText>
          Proteins: {this.calculateValue(protein_g)} g
        </PrimaryText>
        <PrimaryText>
          Total Fats: {this.calculateValue(lipid_total_g)} g
        </PrimaryText>

        <ButtonsWrapper>
          <Button.Group>
            <Button basic color="red" onClick={this.onClickDeleteHandler}>
              Delete
            </Button>
            <Button basic color="blue" onClick={this.props.close}>
              Cancel
            </Button>
            <Button
              basic
              color="violet"
              onClick={this.onClickSaveHandler}
              disabled={
                this.state.value === '' ||
                Number(this.state.value) === 0 ||
                this.state.value === this.props.food.quantity
              }
            >
              Save
            </Button>
          </Button.Group>
        </ButtonsWrapper>
      </Wrapper>
    );
  }
}

export default compose(
  graphql(updateFoodMutation, { name: 'updateFood' }),
  graphql(deleteFoodMutation, { name: 'deleteFood' })
)(FoodItemDetails);
