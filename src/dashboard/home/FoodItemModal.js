import React from 'react';
import { Modal, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

import FoodItemDetails from './FoodItemDetails';

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 0px;
  top: 8px;
`;

const FoodItemModal = ({
  open,
  close,
  food,
  history,
  recordId,
  activeMeal,
  profileId,
  currentDate,
}) => {
  return (
    <Modal
      open={open}
      dimmer={false}
      style={{
        width: '80%',
        marginTop: '-260px',
        marginLeft: '-40%',
        paddingTop: '40px',
      }}
    >
      <Modal.Content>
        <Button onClick={close}>
          <Icon name="close" size="large" color="violet" />
        </Button>

        <FoodItemDetails
          close={close}
          food={food}
          recordId={recordId}
          activeMeal={activeMeal}
          profileId={profileId}
          currentDate={currentDate}
        />
      </Modal.Content>
    </Modal>
  );
};

export default FoodItemModal;
