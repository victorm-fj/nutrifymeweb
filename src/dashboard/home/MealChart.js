import React, { Component } from 'react';
import { VictoryPie } from 'victory';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 120px;
`;

class MealChart extends Component {
  render() {
    const { totalCarbs, totalProts, totalFats } = this.props;
    const data = [
      { x: 'Carbs', y: totalCarbs },
      { x: 'Prots', y: totalProts },
      { x: 'Fats', y: totalFats },
    ];

    return (
      <Wrapper>
        <VictoryPie
          style={{
            labels: { fontSize: 36 },
          }}
          colorScale={['#22dd8f', '#5d72d3', '#ffcc33']}
          padAngle={3}
          labelRadius={95}
          innerRadius={90}
          data={data}
        />
      </Wrapper>
    );
  }
}

export default MealChart;
