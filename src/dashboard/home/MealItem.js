import React, { Component } from 'react';
import { Tab, Table } from 'semantic-ui-react';

import MealResume from './MealResume';
import FoodItem from './FoodItem';

class MealItem extends Component {
  render() {
    return (
      <Tab.Pane>
        <div>
          <h4>
            {this.props.meal}
          </h4>

          <MealResume foods={this.props.foods} />

          <Table celled unstackable selectable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>
                  <p>Food</p>
                </Table.HeaderCell>

                <Table.HeaderCell textAlign="center">
                  <p>Qty</p>
                </Table.HeaderCell>

                <Table.HeaderCell textAlign="center">
                  <p>Unit</p>
                </Table.HeaderCell>

                <Table.HeaderCell textAlign="center">
                  <p>kcal</p>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {this.props.foods.map(food =>
                <FoodItem
                  key={food.food.id}
                  food={food}
                  profileId={this.props.profileId}
                  recordId={this.props.recordId}
                  currentDate={this.props.currentDate}
                />
              )}
            </Table.Body>
          </Table>
        </div>
      </Tab.Pane>
    );
  }
}

export default MealItem;
