import React, { Component } from 'react';
import styled from 'styled-components';

import MealChart from './MealChart';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

class MealResume extends Component {
  calculateTotalValue() {
    let totalKcal = 0,
      totalCarbs = 0,
      totalProts = 0,
      totalFats = 0;
    this.props.foods.forEach(food => {
      const {
        food: {
          kcal,
          carbohydrate_g,
          protein_g,
          lipid_total_g,
          gmwt_1,
          gmwt_desc1,
          gmwt_2,
          gmwt_desc2,
        },
        quantity,
        units,
      } = food;

      if (units === gmwt_desc1) {
        totalKcal += kcal * Number(quantity) * gmwt_1 / 100;
        totalCarbs += carbohydrate_g * Number(quantity) * gmwt_1 / 100;
        totalProts += protein_g * Number(quantity) * gmwt_1 / 100;
        totalFats += lipid_total_g * Number(quantity) * gmwt_1 / 100;
      } else if (units === gmwt_desc2) {
        totalKcal += kcal * Number(quantity) * gmwt_2 / 100;
        totalCarbs += carbohydrate_g * Number(quantity) * gmwt_2 / 100;
        totalProts += protein_g * Number(quantity) * gmwt_2 / 100;
        totalFats += lipid_total_g * Number(quantity) * gmwt_2 / 100;
      } else {
        totalKcal += kcal * Number(quantity) / 100;
        totalCarbs += carbohydrate_g * Number(quantity) / 100;
        totalProts += protein_g * Number(quantity) / 100;
        totalFats += lipid_total_g * Number(quantity) / 100;
      }
    });

    totalKcal = Math.round(totalKcal * 100) / 100;
    totalCarbs = Math.round(totalCarbs * 100) / 100;
    totalProts = Math.round(totalProts * 100) / 100;
    totalFats = Math.round(totalFats * 100) / 100;

    return { totalKcal, totalCarbs, totalProts, totalFats };
  }

  render() {
    const {
      totalKcal,
      totalCarbs,
      totalProts,
      totalFats,
    } = this.calculateTotalValue();

    return (
      <Wrapper>
        <p>
          Total: {totalKcal} kcal
        </p>

        {totalCarbs === 0 && totalProts === 0 && totalFats === 0
          ? <div />
          : <MealChart
              totalCarbs={totalCarbs}
              totalProts={totalProts}
              totalFats={totalFats}
            />}
      </Wrapper>
    );
  }
}

export default MealResume;
