import React from 'react';
import { Card } from 'semantic-ui-react';
import styled from 'styled-components';

import ResumeChart from './ResumeChart';
import MacroResume from './MacroResume';
import { calculateMacrosPercentage, calculateMacrosResume } from '../../utils';

const Wrapper = styled.div`
  flex: 1;
  margin: 15px 0;
  width: 100%;
`;

const ResumeComponent = ({ needs, record: { foods } }) => {
  const totalPercentages = calculateMacrosPercentage(needs, foods);
  const macrosResume = calculateMacrosResume(needs, foods);

  return (
    <Wrapper>
      <Card fluid={true}>
        <Card.Content>
          <Card.Header>
            <h3>Resume</h3>
          </Card.Header>
        </Card.Content>

        <Card.Content>
          <ResumeChart totalPercentages={totalPercentages} />

          <MacroResume macrosResume={macrosResume} />
        </Card.Content>
      </Card>
    </Wrapper>
  );
};

export default ResumeComponent;
