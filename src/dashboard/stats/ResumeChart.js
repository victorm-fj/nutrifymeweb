import React, { Component } from 'react';
import { VictoryChart, VictoryBar } from 'victory';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 225px;
`;

const Legend = styled.p`
  color: #252525;
  font-size: 0.8rem;
  font-family: "Gill Sans", "Gill Sans MT", Ser­avek, "Trebuchet MS", sans-serif;
  margin-bottom: 30px !important;
`;

class ResumeChart extends Component {
  render() {
    const {
      totalKcalPercentage,
      totalCarbsPercentage,
      totalProtsPercentage,
      totalFatsPercentage,
    } = this.props.totalPercentages;
    const data = [
      {
        percentage: totalKcalPercentage,
        macronutrient: 'kcal',
        fill: '#5829bb',
      },
      {
        percentage: totalCarbsPercentage,
        macronutrient: 'carbs',
        fill: '#22dd8f',
      },
      {
        percentage: totalProtsPercentage,
        macronutrient: 'prots',
        fill: '#5d72d3',
      },
      {
        percentage: totalFatsPercentage,
        macronutrient: 'fats',
        fill: '#ffcc33',
      },
    ];

    return (
      <Wrapper>
        <VictoryChart domainPadding={{ y: [25, 25], x: [0, 25] }}>
          <VictoryBar horizontal data={data} y="percentage" x="macronutrient" />
        </VictoryChart>
        <Legend>values as percentages</Legend>
      </Wrapper>
    );
  }
}

export default ResumeChart;
