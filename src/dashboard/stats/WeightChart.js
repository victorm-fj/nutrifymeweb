import moment from 'moment';
import React, { Component } from 'react';
import {
  VictoryChart,
  VictoryGroup,
  VictoryArea,
  VictoryScatter,
} from 'victory';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 225px;
`;

class WeightChart extends Component {
  weightData() {
    const { profiles } = this.props;
    let data = [],
      categories = [],
      rangeData = [],
      domain = [];

    profiles.forEach(profile => {
      const { date, weight, results: { minBW, maxBW } } = profile;
      data.push({ date: moment(date).format('DMMM'), weight: weight });
      categories.push(moment(date).format('DMMM'));
      rangeData.push({
        date: moment(date).format('DMMM'),
        weight: maxBW,
        y0: minBW,
      });
    });

    const profilesLength = profiles.length;
    domain.push(profiles[profilesLength - 1].results.minBW - 10);
    domain.push(profiles[0].weight + 10);

    if (profilesLength === 1) {
      rangeData.push({
        date: '',
        weight: profiles[0].results.maxBW,
        y0: profiles[0].results.minBW,
      });
    }

    return { data, categories, rangeData, domain };
  }

  render() {
    const { data, categories, rangeData, domain } = this.weightData();
    return (
      <Wrapper>
        <VictoryChart>
          <VictoryGroup
            style={{
              data: { strokeWidth: 3, fillOpacity: 0.4 },
            }}
            categories={{ x: categories }}
            domain={{ y: domain }}
          >
            <VictoryArea
              style={{
                parent: {
                  border: '1px solid #ccc',
                },
                data: {
                  fill: '#189963',
                  fillOpacity: 0.7,
                  stroke: '#189963',
                  strokeWidth: 3,
                },
              }}
              data={rangeData}
              x="date"
              y="weight"
            />

            <VictoryArea
              style={{
                parent: {
                  border: '1px solid #ccc',
                },
                data: {
                  fill: 'transparent',
                  fillOpacity: 0.7,
                  stroke: '#5829bb',
                  strokeWidth: 3,
                },
              }}
              data={data}
              x="date"
              y="weight"
            />

            <VictoryScatter
              style={{ data: { fill: '#3a1b7c' } }}
              size={6}
              data={data}
              x="date"
              y="weight"
            />
          </VictoryGroup>
        </VictoryChart>
      </Wrapper>
    );
  }
}

export default WeightChart;
