import React from 'react';
import { Table } from 'semantic-ui-react';

const MacroResume = ({
  macrosResume: {
    kcal: { kcal, eatenKcal, remainingKcal },
    carbs: { carbs, eatenCarbs, remainingCarbs },
    prots: { prots, eatenProts, remainingProts },
    fats: { fats, eatenFats, remainingFats },
  },
}) => {
  return (
    <Table celled>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell />

          <Table.HeaderCell textAlign="center">Recommended</Table.HeaderCell>

          <Table.HeaderCell textAlign="center">Eaten</Table.HeaderCell>

          <Table.HeaderCell textAlign="center">Remaining</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <Table.Row>
          <Table.Cell>Calories (kcal)</Table.Cell>
          <Table.Cell textAlign="center">
            {kcal}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {eatenKcal}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {remainingKcal}
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Carbohydrates (g)</Table.Cell>
          <Table.Cell textAlign="center">
            {carbs}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {eatenCarbs}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {remainingCarbs}
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Proteins (g)</Table.Cell>
          <Table.Cell textAlign="center">
            {prots}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {eatenProts}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {remainingProts}
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Total fats (g)</Table.Cell>
          <Table.Cell textAlign="center">
            {fats}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {eatenFats}
          </Table.Cell>
          <Table.Cell textAlign="center">
            {remainingFats}
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  );
};

export default MacroResume;
