import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import styled from 'styled-components';

import WeightChart from './WeightChart';

const Wrapper = styled.div`
  flex: 1;
  margin: 15px 0;
  width: 100%;
`;

const LegendText = styled.p`
  color: #252525;
  font-size: 0.8rem;
  font-family: "Gill Sans", "Gill Sans MT", Ser­avek, "Trebuchet MS", sans-serif;
`;

const BoxLabel = styled.div.attrs({
  color: props => props.color,
})`
  width: 15px;
  height: 15px;
  background-color: ${props => props.color};
  border: 1px solid ${props => props.color};
  margin-left: 10px;
  margin-right: 5px;
`;

const BoxesWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

class WeightTracker extends Component {
  render() {
    return (
      <Wrapper>
        <Card fluid={true}>
          <Card.Content>
            <Card.Header>
              <h3>Weight Tracker</h3>
            </Card.Header>
          </Card.Content>

          <Card.Content>
            <WeightChart profiles={this.props.profiles} />

            <BoxesWrapper>
              <BoxLabel color="#189963" />
              <LegendText>Ideal Body Weight range</LegendText>
              <BoxLabel color="#5829bb" />
              <LegendText>Current Body Weight</LegendText>
            </BoxesWrapper>
          </Card.Content>
        </Card>
      </Wrapper>
    );
  }
}

export default WeightTracker;
