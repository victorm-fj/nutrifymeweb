import React from 'react';
import styled from 'styled-components';

import WeightTracker from './WeightTracker';
import ResumeComponent from './ResumeComponent';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width: 90%;
  margin: 0 auto 66px;

  @media (min-width: 800px) {
    width: 80%;
  }
`;

const Stats = ({ profile, record, profiles }) => {
  return (
    <Wrapper>
      <WeightTracker profiles={profiles} />

      <ResumeComponent needs={profile.needs} record={record} />
    </Wrapper>
  );
};

export default Stats;
