import React, { Component } from 'react';
import styled from 'styled-components';
import { graphql, compose } from 'react-apollo';
import { Dimmer, Loader } from 'semantic-ui-react';

import recordQuery from '../queries/CurrentRecord';
import profilesQuery from '../queries/ProfileList';
import Dashboard from './Dashboard';
import Header from './Header';
import DateComponent from './DateComponent';

const Wrapper = styled.div`
  display: flex;
  min-height: 100vh;
  width: 100%;
  flex-direction: column;
  align-items: center;
  position: relative;
`;

class DashboardContainer extends Component {
  render() {
    const {
      profileQuery,
      recordQuery,
      profilesQuery,
      history,
      match,
      currentDate,
      setDate,
      activeMeal,
    } = this.props;

    if (recordQuery.loading || profilesQuery.loading) {
      return (
        <Dimmer active inverted>
          <Loader inverted>Loading</Loader>
        </Dimmer>
      );
    }

    return (
      <Wrapper>
        <Header />

        <DateComponent
          record={recordQuery.record}
          currentDate={currentDate}
          setDate={setDate}
          history={history}
          match={match}
          profiles={profilesQuery.profiles}
        />

        <Dashboard
          profile={profileQuery.profile}
          record={recordQuery.record}
          history={history}
          match={match}
          currentDate={currentDate}
          activeMeal={activeMeal}
          profiles={profilesQuery.profiles}
        />
      </Wrapper>
    );
  }
}

export default compose(
  graphql(recordQuery, {
    name: 'recordQuery',
    options: ownProps => ({
      variables: {
        profileId: ownProps.profileQuery.profile.id,
        date: ownProps.currentDate,
      },
    }),
  }),
  graphql(profilesQuery, { name: 'profilesQuery' })
)(DashboardContainer);
