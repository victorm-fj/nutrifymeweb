import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 90%;
  margin: 30px auto 86px;
`;

const LinksList = styled.ul`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 200px;
  padding: 0;
  list-style: none;
`;

const ALink = styled(NavLink)`
  font-size: 14px;
  color: #757575;
  cursor: pointer;
`;

const Info = () => {
  return (
    <Wrapper>
      <LinksList>
        <li style={{ margin: '10px' }}>
          <ALink to="/about">About</ALink>
        </li>
        <li style={{ margin: '10px' }}>
          <ALink to="/terms">Terms of Use</ALink>
        </li>
        <li style={{ margin: '10px' }}>
          <ALink to="/privacy">Privacy Police</ALink>
        </li>
        <li style={{ margin: '10px' }}>
          <ALink to="/cookie">Cookie Use</ALink>
        </li>
      </LinksList>
    </Wrapper>
  );
};

export default Info;
