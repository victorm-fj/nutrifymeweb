import moment from 'moment';
import React, { Component } from 'react';
import styled from 'styled-components';
import { Button } from 'semantic-ui-react';

const Wrapper = styled.div`
  width: 100%;
  height: 56px;
  margin-top: 10px;
  padding: 16px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

const format = 'YYYY-MM-DD';

class DateComponent extends Component {
  renderDateLabel() {
    const { record: { date } } = this.props;
    const today = moment().format(format);
    const yesterday = moment().add(-1, 'days').format(format);
    const tomorrow = moment().add(1, 'days').format(format);

    let label;

    if (moment(today, format).isSame(date)) {
      label = 'Today';
    } else if (moment(yesterday, format).isSame(date)) {
      label = 'Yesterday';
    } else if (moment(tomorrow, format).isSame(date)) {
      label = 'Tomorrow';
    } else {
      label = moment(date, format).format('dddd');
    }

    return label;
  }

  onDateBackward = () => {
    const firstProfileDate = this.props.profiles[0].date;
    const { currentDate } = this.props;
    const newDate = moment(currentDate, format).add(-1, 'days').format(format);

    if (newDate >= firstProfileDate) {
      this.props.setDate(newDate);
    }
  };

  onDateForward = () => {
    const { currentDate } = this.props;
    const newDate = moment(currentDate, format).add(1, 'days').format(format);
    this.props.setDate(newDate);
  };

  render() {
    if (
      this.props.history.location.pathname === `${this.props.match.url}/info`
    ) {
      return <div />;
    }

    return (
      <Wrapper>
        <Button
          icon="chevron left"
          onClick={this.onDateBackward}
          disabled={this.props.currentDate === this.props.profiles[0].date}
        />
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <h4 style={{ marginBottom: '0px' }}>
            {this.renderDateLabel()}
          </h4>
          <p style={{ textAlign: 'center' }}>
            {moment(this.props.record.date).format('D MMM')}
          </p>
        </div>
        <Button icon="chevron right" onClick={this.onDateForward} />
      </Wrapper>
    );
  }
}

export default DateComponent;
