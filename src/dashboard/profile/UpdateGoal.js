import React, { Component } from 'react';
import { Button, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider';
import { graphql } from 'react-apollo';

import mutation from '../../mutations/UpdateProfile';
import query from '../../queries/UserProfile';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 60px;
  margin-bottom: 20px;
`;

const SliderWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  height: 140px;
  width: 75%;
  padding-top: 40px;
`;

const Handle = Slider.Handle;

class UpdateGoal extends Component {
  state = {
    goal: this.props.profile.goal,
    sliderValue: this.props.profile.calRestriction,
    saving: false,
  };

  onHandle = props => {
    const { value, ...restProps } = props;
    return <Handle value={value} {...restProps} />;
  };

  onChangeHandler = value => {
    this.setState({ sliderValue: value });
  };

  onClickHandler = event => {
    event.target.textContent === 'Weight maintenance'
      ? this.setState({ goal: 0 })
      : this.setState({ goal: 1 });
  };

  onSaveHandler = () => {
    const { goal, sliderValue } = this.state;
    const restriction = sliderValue === 0 ? 500 : sliderValue;
    const calRestriction = goal === 0 ? 0 : restriction;

    this.setState({ saving: true });

    this.props
      .mutate({
        variables: {
          id: this.props.profile.id,
          date: this.props.currentDate,
          goal,
          calRestriction,
        },
        refetchQueries: [
          {
            query,
            variables: {
              date: this.props.currentDate,
            },
          },
        ],
      })
      .then(() => {
        this.setState({ saving: false });
        this.props.close();
      })
      .catch(err => console.log(err));
  };

  renderSlider() {
    const { goal, sliderValue } = this.state;
    const calRestriction = sliderValue === 0 ? 500 : sliderValue;

    if (goal === 1) {
      return (
        <SliderWrapper>
          <Slider
            min={500}
            max={1000}
            defaultValue={calRestriction}
            handle={this.onHandle}
            trackStyle={{ backgroundColor: '#5829bb' }}
            handleStyle={{ borderColor: '#5829bb' }}
            step={100}
            onChange={this.onChangeHandler}
          />

          <div style={{ textAlign: 'center' }}>
            <p style={{ marginBottom: '0px' }}>
              Daily calorie restriction: {calRestriction} kcal
            </p>
            <p>
              Approximate weight loss: {calRestriction / 1000} kg
            </p>
          </div>
        </SliderWrapper>
      );
    }

    return <div />;
  }

  render() {
    return (
      <Wrapper>
        {this.state.saving
          ? <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          : <div />}

        <Button.Group>
          <Button onClick={this.onClickHandler} active={this.state.goal === 0}>
            Weight maintenance
          </Button>
          <Button onClick={this.onClickHandler} active={this.state.goal === 1}>
            Lose weight
          </Button>
        </Button.Group>

        {this.renderSlider()}

        <ButtonsWrapper>
          <Button basic color="red" onClick={this.props.close}>
            Cancel
          </Button>
          <Button basic color="violet" onClick={this.onSaveHandler}>
            Save
          </Button>
        </ButtonsWrapper>
      </Wrapper>
    );
  }
}

export default graphql(mutation)(UpdateGoal);
