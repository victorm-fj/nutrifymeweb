import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';
import styled from 'styled-components';

import bmiIntervals from './bmiIntervals.json';

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px 0;
`;

const renderPane = ({ interval, legend }) =>
  <Tab.Pane>
    <div>
      <h4 style={{ textAlign: 'center', marginBottom: '0px' }}>
        {interval}
      </h4>
      <p style={{ textAlign: 'center', marginBottom: '10px' }}>
        kg/m<sup>2</sup>
      </p>
      <p style={{ textAlign: 'center' }}>
        {legend}
      </p>
    </div>
  </Tab.Pane>;

const panes = bmiIntervals.map(interval => {
  return {
    menuItem: interval.id,
    render: () => renderPane(interval),
  };
});

class BMIComponent extends Component {
  activeIndex() {
    let activeIndex = 0;
    bmiIntervals.forEach((interval, index) => {
      if (interval.interval === this.props.interval)
        activeIndex = interval.id - 1;
    });
    return activeIndex;
  }

  render() {
    const activeIndex = this.activeIndex();
    return (
      <Wrapper>
        <Tab
          menu={{ pointing: true }}
          panes={panes}
          activeIndex={activeIndex}
        />
      </Wrapper>
    );
  }
}

export default BMIComponent;
