import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';
import styled from 'styled-components';

import UserDataModal from './UserDataModal';

const InnerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

class UserData extends Component {
  state = {
    open: false,
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      profile: { age, weight, height, gender },
      currentDate,
    } = this.props;
    return (
      <Table celled unstackable>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <InnerWrapper>
                Age (years)
                <Button
                  basic
                  color="violet"
                  size="mini"
                  onClick={() => this.setState({ open: true })}
                >
                  Edit
                </Button>
              </InnerWrapper>
            </Table.Cell>
            <Table.Cell textAlign="center" width={4}>
              {age}
            </Table.Cell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>Weight (kg)</Table.Cell>
            <Table.Cell textAlign="center" width={4}>
              {weight}
            </Table.Cell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>Height (cm)</Table.Cell>
            <Table.Cell textAlign="center" width={4}>
              {height}
            </Table.Cell>
          </Table.Row>

          <Table.Row>
            <Table.Cell>Gender</Table.Cell>
            <Table.Cell textAlign="center" width={4}>
              {gender === 'male' ? 'Male' : 'Female'}
            </Table.Cell>
          </Table.Row>
        </Table.Body>

        <UserDataModal
          open={this.state.open}
          close={this.closeModal}
          profile={this.props.profile}
          currentDate={currentDate}
        />
      </Table>
    );
  }
}

export default UserData;
