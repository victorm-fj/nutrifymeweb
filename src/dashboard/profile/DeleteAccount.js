import React, { Component } from 'react';
import { Button, Modal, Icon, Dimmer, Loader } from 'semantic-ui-react';
import styled from 'styled-components';
import { ApolloClient, withApollo, graphql } from 'react-apollo';
import PropTypes from 'prop-types';

import mutation from '../../mutations/DeleteAccount';

const Wrapper = styled.div`
  text-align: center;
  margin: 40px;
`;

const CloseButton = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 0px;
  top: 8px;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
  margin-bottom: 20px;
`;

class DeleteAccount extends Component {
  state = { open: false, deleting: false };

  toggleModal = () => {
    this.setState({ open: !this.state.open });
  };

  onDeleteAccount = () => {
    this.setState({ deleting: true });
    this.toggleModal();
    this.props
      .mutate()
      .then(() => this.props.client.resetStore())
      .catch(err => console.error(err));
  };

  render() {
    return (
      <Wrapper>
        {this.state.deleting
          ? <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          : <div />}

        <Button basic size="large" color="violet" onClick={this.toggleModal}>
          Delete Account
        </Button>

        <Modal
          open={this.state.open}
          dimmer={false}
          style={{
            width: '80%',
            marginTop: '-120px',
            marginLeft: '-40%',
            paddingTop: '40px',
          }}
        >
          <Modal.Content>
            <CloseButton onClick={this.toggleModal}>
              <Icon name="close" size="large" color="violet" />
            </CloseButton>

            <p>
              By deleting your account all your information will be erased from
              our DataBase, with no posibility of data recover.
            </p>

            <p>Are you sure you want to delete your account?</p>

            <ButtonsWrapper>
              <Button.Group>
                <Button basic color="blue" onClick={this.toggleModal}>
                  Cancel
                </Button>
                <Button basic color="red" onClick={this.onDeleteAccount}>
                  Delete
                </Button>
              </Button.Group>
            </ButtonsWrapper>
          </Modal.Content>
        </Modal>
      </Wrapper>
    );
  }
}

DeleteAccount.propTypes = {
  client: PropTypes.instanceOf(ApolloClient),
};

export default withApollo(graphql(mutation)(DeleteAccount));
