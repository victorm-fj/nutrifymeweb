import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';
import styled from 'styled-components';

import ActivityLevelModal from './ActivityLevelModal';

const InnerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const activityLevels = [
  'Very Light',
  'Light',
  'Moderate',
  'Heavy',
  'Exceptional',
];

class ActivityLevel extends Component {
  state = {
    open: false,
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <Table celled unstackable>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <InnerWrapper>
                Physical Activity Level
                <Button
                  basic
                  color="violet"
                  size="mini"
                  onClick={() => this.setState({ open: true })}
                >
                  Edit
                </Button>
              </InnerWrapper>
            </Table.Cell>
            <Table.Cell textAlign="center" width={4}>
              {activityLevels[this.props.profile.activityLevel - 1]}
            </Table.Cell>
          </Table.Row>
        </Table.Body>

        <ActivityLevelModal
          open={this.state.open}
          close={this.closeModal}
          profile={this.props.profile}
          currentDate={this.props.currentDate}
        />
      </Table>
    );
  }
}

export default ActivityLevel;
