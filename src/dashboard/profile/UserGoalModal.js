import React, { Component } from 'react';
import { Modal, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

import UpdateGoal from './UpdateGoal';

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 0px;
  top: 8px;
`;

class UserGoalModal extends Component {
  render() {
    return (
      <Modal
        open={this.props.open}
        dimmer={false}
        style={{
          width: '80%',
          marginTop: '-200px',
          marginLeft: '-40%',
          paddingTop: '40px',
        }}
      >
        <Modal.Content>
          <Button onClick={this.props.close}>
            <Icon name="close" size="large" color="violet" />
          </Button>

          <UpdateGoal
            profile={this.props.profile}
            close={this.props.close}
            currentDate={this.props.currentDate}
          />
        </Modal.Content>
      </Modal>
    );
  }
}

export default UserGoalModal;
