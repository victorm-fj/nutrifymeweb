import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';
import styled from 'styled-components';

import UserGoalModal from './UserGoalModal';

const InnerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

class UserGoal extends Component {
  state = {
    open: false,
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <Table celled unstackable>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <InnerWrapper>
                Goal
                <Button
                  basic
                  color="violet"
                  size="mini"
                  onClick={() => this.setState({ open: true })}
                >
                  Edit
                </Button>
              </InnerWrapper>
            </Table.Cell>
            <Table.Cell textAlign="center" width={4}>
              {this.props.profile.goal === 1
                ? 'Lose weight'
                : 'Weight maintenance'}
            </Table.Cell>
          </Table.Row>
        </Table.Body>

        <UserGoalModal
          open={this.state.open}
          close={this.closeModal}
          profile={this.props.profile}
          currentDate={this.props.currentDate}
        />
      </Table>
    );
  }
}

export default UserGoal;
