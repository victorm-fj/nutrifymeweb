import React from 'react';
import { Table } from 'semantic-ui-react';

const ResultsComponent = ({
  results: { bmi, minBW, maxBW, idealBW },
  currentBW,
}) => {
  return (
    <Table celled unstackable>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Body Max Index (BMI)</Table.Cell>
          <Table.Cell textAlign="center">
            {bmi} kg/m<sup>2</sup>
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Ideal Body Weight Range</Table.Cell>
          <Table.Cell textAlign="center">
            {minBW} kg - {maxBW} kg
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Ideal Body Weight (IBW)</Table.Cell>
          <Table.Cell textAlign="center">
            {idealBW} kg
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Current Body Weight</Table.Cell>
          <Table.Cell textAlign="center">
            {currentBW} kg
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  );
};

export default ResultsComponent;
