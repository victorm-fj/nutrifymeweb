import React from 'react';
import { Card } from 'semantic-ui-react';
import styled from 'styled-components';

import BMIComponent from './BMIComponent';
import ResultsComponent from './ResultsComponent';

const Wrapper = styled.div`
  flex: 1;
  margin: 15px 0;
  width: 100%;
`;

const WeightStatus = ({ profile: { results, weight } }) => {
  return (
    <Wrapper>
      <Card fluid={true}>
        <Card.Content>
          <Card.Header>
            <h3>Weight Status</h3>
          </Card.Header>
        </Card.Content>

        <Card.Content>
          <p>BMI status</p>

          <BMIComponent interval={results.interval} />

          <ResultsComponent results={results} currentBW={weight} />
        </Card.Content>
      </Card>
    </Wrapper>
  );
};

export default WeightStatus;
