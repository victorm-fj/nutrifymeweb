import React from 'react';
import { Modal, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

import PersonalData from '../../components/PersonalData';

const Button = styled.button`
  background-color: transparent;
  border: none;
  outline: none;
  position: absolute;
  right: 0px;
  top: 8px;
`;

const UserDataModal = ({ open, close, profile, currentDate }) => {
  return (
    <Modal
      open={open}
      dimmer={false}
      style={{
        width: '80%',
        marginTop: '-240px',
        marginLeft: '-40%',
        paddingTop: '40px',
      }}
    >
      <Modal.Content>
        <Button onClick={close}>
          <Icon name="close" size="large" color="violet" />
        </Button>

        <PersonalData
          close={close}
          profile={profile}
          currentDate={currentDate}
        />
      </Modal.Content>
    </Modal>
  );
};

export default UserDataModal;
