import React from 'react';

import UserData from './UserData';
import ActivityLevel from './ActivityLevel';
import UserGoal from './UserGoal';

const UserProfile = ({ profile, currentDate }) => {
  return (
    <div>
      <UserData profile={profile} currentDate={currentDate} />

      <ActivityLevel profile={profile} currentDate={currentDate} />

      <UserGoal profile={profile} currentDate={currentDate} />
    </div>
  );
};

export default UserProfile;
