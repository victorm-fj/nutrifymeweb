import React from 'react';
import styled from 'styled-components';

import WeightStatus from './WeightStatus';
import ProfileComponent from './ProfileComponent';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width: 90%;
  margin: 0 auto 66px;

  @media (min-width: 800px) {
    width: 80%;
  }
`;

const Profile = ({ profile, currentDate }) => {
  return (
    <Wrapper>
      <WeightStatus profile={profile} />

      <ProfileComponent profile={profile} currentDate={currentDate} />
    </Wrapper>
  );
};

export default Profile;
