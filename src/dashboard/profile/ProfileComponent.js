import React from 'react';
import { Card } from 'semantic-ui-react';
import styled from 'styled-components';

import UserProfile from './UserProfile';
import DeleteAccount from './DeleteAccount';

const Wrapper = styled.div`
  flex: 1;
  margin: 15px 0;
  width: 100%;
`;

const ProfileComponent = ({ profile, currentDate }) => {
  return (
    <Wrapper>
      <Card fluid={true}>
        <Card.Content>
          <Card.Header>
            <h3>Profile</h3>
          </Card.Header>
        </Card.Content>

        <Card.Content>
          <UserProfile profile={profile} currentDate={currentDate} />
        </Card.Content>
      </Card>

      <DeleteAccount />
    </Wrapper>
  );
};

export default ProfileComponent;
