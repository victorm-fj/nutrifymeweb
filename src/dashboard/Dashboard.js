import React, { Component } from 'react';
import { Route, Redirect, NavLink } from 'react-router-dom';
import { Icon } from 'semantic-ui-react';
import styled from 'styled-components';

import Home from './home';
import Profile from './profile';
import Stats from './stats';
import Info from './Info';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  margin-top: 10px;
  position: relative;

  @media (min-width: 800px) {
    width: 80%;
  }

  @media (min-width: 1100px) {
    width: 70%;
  }
`;

const SideBar = styled.div`
  width: 100%;
  height: 56px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  left: 0;
  bottom: 0;
  box-shadow: 0px 0px 10px 0px rgba(212, 212, 213, 1);
  background-color: #fff;

  @media (min-width: 800px) {
    flex-direction: column;
    position: fixed;
    top: 76px;
    left: 0;
    width: 56px;
    min-height: 300px;
  }
`;

const NavLinkBox = styled(NavLink)`
  align-self: stretch;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 25%;
  border: none;
  background: none;

  &:focus {
    outline: 0;
  }

  &:active {
    background-color: #d4d4d5;
  }

  @media (min-width: 800px) {
    width: 100%;
    height: 25%;
  }
`;

class Dashboard extends Component {
  render() {
    const { match, history } = this.props;

    return (
      <Wrapper>
        <Route
          exact
          path={match.path}
          render={() => <Redirect to={`${match.path}/home`} />}
        />
        <Route
          path={`${match.path}/home`}
          render={() => <Home {...this.props} />}
        />
        <Route
          path={`${match.path}/stats`}
          render={() => <Stats {...this.props} />}
        />
        <Route
          path={`${match.path}/profile`}
          render={() => <Profile {...this.props} />}
        />
        <Route path={`${match.path}/info`} render={() => <Info />} />

        <SideBar>
          <NavLinkBox to={`${match.url}/home`}>
            <Icon
              name="home"
              size="large"
              className={
                history.location.pathname === `${match.url}/home`
                  ? 'iActive'
                  : 'grey'
              }
            />
          </NavLinkBox>

          <NavLinkBox to={`${match.url}/stats`}>
            <Icon
              name="bar chart"
              size="large"
              className={
                history.location.pathname === `${match.url}/stats`
                  ? 'iActive'
                  : 'grey'
              }
            />
          </NavLinkBox>

          <NavLinkBox to={`${match.url}/profile`}>
            <Icon
              name="user"
              size="large"
              className={
                history.location.pathname === `${match.url}/profile`
                  ? 'iActive'
                  : 'grey'
              }
            />
          </NavLinkBox>

          <NavLinkBox to={`${match.url}/info`}>
            <Icon
              name="info circle"
              size="large"
              className={
                history.location.pathname === `${match.url}/info`
                  ? 'iActive'
                  : 'grey'
              }
            />
          </NavLinkBox>
        </SideBar>
      </Wrapper>
    );
  }
}

export default Dashboard;
