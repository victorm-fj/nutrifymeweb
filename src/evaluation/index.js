import React, { Component } from 'react';
import { ViewPager, Frame, Track, View } from 'react-view-pager';
import styled from 'styled-components';
import { Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';

import mutation from '../mutations/CreateProfile';
import query from '../queries/UserProfile';
import UnitPicker from './UnitPicker';
import PersonalData from './PersonalData';
import PhysicalActivity from './PhysicalActivity';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 100vh;
  padding-top: 40px;
  position: relative;
`;

const NavButtons = styled.nav`
  display: flex;
  justify-content: center;
  margin: 60px;
`;

class SliderComponent extends Component {
  state = {
    currentView: 0,
    error: '',
  };

  onViewChangeHandler = currentIndex => {
    this.setState({ currentView: currentIndex[0] });
  };

  onLastNextHandler = () => {
    const {
      date,
      unitSystem,
      activityLevel,
      age,
      weight,
      height,
      gender,
    } = this.props;

    if (this.state.currentView === 1) {
      if (age > 0 && weight > 0 && height > 0) {
        this.setState({ error: '' });
        this.track.next();
      } else {
        this.setState({ error: 'Please fill in all form fields' });
      }
    } else if (this.state.currentView === 2) {
      this.props.mutate({
        variables: {
          date,
          unitSystem,
          activityLevel,
          age,
          weight,
          height,
          gender,
        },
        refetchQueries: [{ query, variables: { date } }],
      });
    } else {
      this.track.next();
    }
  };

  render() {
    return (
      <Wrapper>
        <ViewPager tag="main">
          <Frame accessibility={false}>
            <Track
              ref={c => (this.track = c)}
              viewsToShow={1}
              infinite={false}
              swipe={false}
              currentView={this.state.currentView}
              onViewChange={this.onViewChangeHandler}
            >
              <View>
                <UnitPicker />
              </View>

              <View>
                <PersonalData error={this.state.error} />
              </View>

              <View>
                <PhysicalActivity />
              </View>
            </Track>
          </Frame>

          <NavButtons>
            <Button
              basic
              onClick={() => this.track.prev()}
              disabled={this.state.currentView === 0}
              color="blue"
            >
              Prev
            </Button>

            <Button basic onClick={this.onLastNextHandler} color="violet">
              Next
            </Button>
          </NavButtons>
        </ViewPager>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => {
  const {
    date,
    unitSystem,
    activityLevel,
    age,
    weight,
    height,
    gender,
  } = state.profile;

  return {
    date,
    unitSystem,
    activityLevel,
    age,
    weight,
    height,
    gender,
  };
};

export default compose(connect(mapStateToProps), graphql(mutation))(
  SliderComponent
);
