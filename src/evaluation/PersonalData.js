import React, { Component } from 'react';
import { Form, Input, Select } from 'semantic-ui-react';
import styled from 'styled-components';
import _ from 'lodash';
import { connect } from 'react-redux';

import { createProfile } from '../actions';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-bottom: 60px;
`;

const ErrorText = styled.p`
  font-size: 0.9rem;
  color: #c70039;
  font-weight: bold;
  padding-top: 3px;
  text-align: center;
`;

const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
];

class PersonalData extends Component {
  setValue = (key, value) => {
    if (value === '') {
      this.props.createProfile({ [key]: 0 });
    }

    const num = Number(value);
    if (_.isFinite(num) && num > 0) {
      this.props.createProfile({ [key]: num });
    }
  };

  onChangeHandler = (event, data) => {
    const { value, placeholder } = data;
    if (placeholder === 'years') this.setValue('age', value);
    else if (placeholder === 'kg') this.setValue('weight', value);
    else if (placeholder === 'cm') this.setValue('height', value);
    else this.props.createProfile({ gender: value });
  };

  render() {
    return (
      <Wrapper>
        <h4 style={{ padding: '0 10px 20px' }}>Fill in the form:</h4>

        <Form size="small">
          <label>Age</label>
          <Form.Field
            control={Input}
            placeholder="years"
            onChange={this.onChangeHandler}
          />

          <label>Weight</label>
          <Form.Field
            control={Input}
            placeholder="kg"
            onChange={this.onChangeHandler}
          />

          <label>Height</label>
          <Form.Field
            control={Input}
            placeholder="cm"
            onChange={this.onChangeHandler}
          />

          <label>Gender</label>
          <Form.Field
            control={Select}
            options={options}
            placeholder="gender"
            onChange={this.onChangeHandler}
            defaultValue="male"
          />

          <ErrorText>
            {this.props.error}
          </ErrorText>
        </Form>
      </Wrapper>
    );
  }
}

export default connect(null, { createProfile })(PersonalData);
