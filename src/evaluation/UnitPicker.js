import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { createProfile } from '../actions';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

class UnitPicker extends Component {
  onClickHandler = event => {
    event.target.textContent === 'Metric'
      ? this.props.createProfile({ unitSystem: 0 })
      : this.props.createProfile({ unitSystem: 1 });
  };

  render() {
    return (
      <Wrapper>
        <h4 style={{ padding: '0 10px 20px' }}>
          Choose preferred unit system:
        </h4>

        <Button.Group>
          {/* Metric system is selected by default, so button must be 'active' as well */}
          <Button
            onClick={this.onClickHandler}
            active={this.props.unitSystem === 0}
          >
            Metric
          </Button>
          <Button
            onClick={this.onClickHandler}
            active={this.props.unitSystem === 1}
          >
            US/Imperial
          </Button>
        </Button.Group>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => {
  const { unitSystem } = state.profile;
  return { unitSystem };
};

export default connect(mapStateToProps, { createProfile })(UnitPicker);
