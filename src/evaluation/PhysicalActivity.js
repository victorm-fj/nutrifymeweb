import React, { Component } from 'react';
import { Tab } from 'semantic-ui-react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { createProfile } from '../actions';
import activityLevels from '../data/activityLevels.json';

const Wrapper = styled.div`
  flex: 1;
  width: 90%;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;

  @media (min-width: 500px) {
    width: 70%;
  }

  @media (min-width: 800px) {
    width: 50%;
  }

  @media (min-width: 950px) {
    width: 40%;
  }

  @media (min-width: 1440px) {
    width: 30%;
  }
`;

const renderPane = ({ level, description }) =>
  <Tab.Pane style={{ color: '#757575' }}>
    <div>
      <h4>
        {level}
      </h4>
      <p style={{ textAlign: 'justify' }}>
        {description}
      </p>
    </div>
  </Tab.Pane>;

const panes = activityLevels.map(level => {
  return {
    menuItem: level.id,
    render: () => renderPane(level),
  };
});

class PhysicalActivity extends Component {
  onTabChangeHandler = (event, data) => {
    this.props.createProfile({ activityLevel: data.activeIndex + 1 });
  };

  render() {
    return (
      <Wrapper>
        <h4 style={{ padding: '0 10px 20px' }}>Choose your activity level:</h4>

        <Tab
          menu={{ pointing: true }}
          panes={panes}
          onTabChange={this.onTabChangeHandler}
        />
      </Wrapper>
    );
  }
}

export default connect(null, { createProfile })(PhysicalActivity);
